import React from "react";
import CurrencyFormat from "react-currency-format";

export default function (props) {
  const regularColors = props.val >= 0.0 ? "#80cf00" : "#fd517d";
  const inverseColors = props.val >= 0.0 ? "#fd517d" : "#80cf00";
  return (
    <span
      style={{
        color: `${props.inverseColors ? inverseColors : regularColors}`,
      }}
    >
      {props.val >= 0.0 ? "+" : "-"}
      <CurrencyFormat
        value={props.val?.toFixed(props.decimalScale || 2)}
        displayType="text"
        thousandSeparator={true}
        prefix={props.prefix || ""}
        decimalScale={props.decimalScale || 2}
      />
      {props.children}
    </span>
  );
}
