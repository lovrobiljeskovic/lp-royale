import React, { useEffect } from "react";
import { useLocalStorage } from "../helpers/util";
import { Input } from "reactstrap";
import { toast } from "react-toastify";

export default function SwitchMode() {
  const [themeMode, setThemeMode] = useLocalStorage("theme");
  const [accessToken] = useLocalStorage("accessToken");

  useEffect(() => {
    document.body.classList.add(themeMode);
  }, [themeMode]);

  function changeTheme() {
    if (!accessToken) {
      toast.error(
        "Unable to change theme. Please ensure you are signed in and have VISION tokens.",
        {
          position: toast.POSITION.TOP_CENTER,
          autoClose: 6000,
        }
      );
      return;
    }
    if (themeMode === "light") {
      document.body.classList.remove("light");
      document.body.classList.add("dark-only");
      setThemeMode("dark-only");
    } else {
      document.body.classList.remove("dark-only");
      document.body.classList.add("light");
      setThemeMode("light");
    }
  }

  return (
    <div>
      <span className="badge badge-pill badge-primary badge-theme-switcher">
        PRO
      </span>
      <Input
        type="checkbox"
        onChange={() => changeTheme()}
        className="react-switch-checkbox"
        id="react-switch-new"
        checked={themeMode === "dark-only"}
      />
      <label
        className={
          "react-switch-label " +
          (themeMode === "light" ? "switch-light-bg" : "switch-dark-bg")
        }
        htmlFor="react-switch-new"
      >
        <span className="react-switch-button" />
      </label>
    </div>
  );
}
