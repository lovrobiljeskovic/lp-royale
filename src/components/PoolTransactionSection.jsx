import React from "react";
import { Card, CardHeader, Col, Row, Table } from "reactstrap";
import { displayPrettyNumber } from "../helpers/util";
import PctGainLossSpan from "./PctGainLossSpan";
import PoolTransactionsChart from "./PoolTransactionChart";
import { Link } from "react-router-dom";
import styled from "styled-components";

const CardContainer = styled.div`
  .card {
    margin-bottom: 60px;
    background: #ffffff;
    border: 1px solid #9fbacf;
    box-sizing: border-box;
    box-shadow: 0px 4px 0px rgba(4, 43, 78, 0.14);
    border-radius: 0px 0px 24px 24px;
  }
  .table {
    margin-bottom: 0px !important;
  }
  .table td {
    white-space: nowrap;
    border-top: 1px solid #9fbacf;
    font-size: 14px;
    text-align: center;
    font-family: "Nunito Sans", sans-serif;
    font-weight: 900;
    word-wrap: break-word;
    color: #1b3155;
    padding-top: 20px !important;
    padding-bottom: 20px !important;
    padding-left: 20px !important;
    padding-right: 20px !important;
    text-align: center;
    vertical-align: middle;
  }
  .table thead th {
    border-bottom: 1px solid #9fbacf;
    font-size: 12px;
    max-width: 180px;
    text-align: center;
    text-transform: uppercase;
    color: #1b3155;
    font-family: "Nunito Sans", sans-serif;
  }
  .table th {
    color: #1b3155;
    white-space: nowrap;
    border-top: 1px solid #9fbacf;
    padding-top: 20px !important;
    padding-bottom: 20px !important;
    padding-left: 20px !important;
    padding-right: 20px !important;
  }
`;

export default function (props) {
  const poolInfo = props.poolInfo;
  const exchangePoolInfo = props.exchangePoolInfo;

  function renderPctGainForPeriod(prev, curr) {
    const pct = ((parseFloat(curr) - prev) / prev) * 100;
    return <PctGainLossSpan val={pct}> %</PctGainLossSpan>;
  }

  function renderPoolTransactions() {
    function renderRow(dateLabel, txns, change) {
      return (
        <tr>
          <th scope="row">{dateLabel}</th>
          {txns === -1 ? (
            <td colSpan={2} className="centered">
              <Link to="/membership">[go pro]</Link>
            </td>
          ) : (
            <>
              <td>{parseInt(txns, 10)} Txs</td>
              <td>{change || "-"}</td>
            </>
          )}
        </tr>
      );
    }

    return (
      <div>
        <h6
          style={{
            fontFamily: "Nunito Sans, sans-serif",
            fontSize: 18,
            fontWeight: 900,
            color: "#042B4E",
          }}
        >
          Average # of transactions
        </h6>
        <p
          style={{
            fontFamily: "Nunito Sans, sans-serif",
            fontSize: 14,
            fontWeight: 900,
            color: "#042B4E",
            marginBottom: 12,
          }}
        >
          A growing average transactions count over time is sign of an increase
          in trade volume
        </p>
        <CardContainer>
          <Card style={{ borderRadius: 24 }}>
            <div className="card-block row">
              <Col sm="12" lg="12" xl="12">
                <div className="tableresponsive" style={{ overflowX: "auto" }}>
                  <Table>
                    <thead>
                      <tr>
                        <th scope="col"></th>
                        <th scope="col">Avg # of tx</th>
                        <th scope="col">vs. Yesterday</th>
                      </tr>
                    </thead>
                    <tbody>
                      {renderRow(
                        "Yesterday",
                        poolInfo.avg_txns_size["1d"],
                        null
                      )}
                      {poolInfo.pool_age_days >= 7
                        ? renderRow(
                            "Daily Avg Last 7 Day",
                            poolInfo.avg_txns_size["7d"],
                            renderPctGainForPeriod(
                              poolInfo.avg_txns_size["7d"],
                              poolInfo.avg_txns_size["1d"]
                            )
                          )
                        : null}
                      {poolInfo.pool_age_days >= 14
                        ? renderRow(
                            "Daily Avg Last 14 Days",
                            poolInfo.avg_txns_size["14d"],
                            renderPctGainForPeriod(
                              poolInfo.avg_txns_size["14d"],
                              poolInfo.avg_txns_size["1d"]
                            )
                          )
                        : null}
                      {poolInfo.pool_age_days >= 30
                        ? renderRow(
                            "Daily Avg Last 30 Days",
                            poolInfo.avg_txns_size["30d"],
                            renderPctGainForPeriod(
                              poolInfo.avg_txns_size["30d"],
                              poolInfo.avg_txns_size["1d"]
                            )
                          )
                        : null}
                      {poolInfo.pool_age_days >= 30
                        ? renderRow(
                            "Daily Avg Since Inception",
                            poolInfo.avg_txns_size["inception"],
                            renderPctGainForPeriod(
                              poolInfo.avg_txns_size["inception"],
                              poolInfo.avg_txns_size["1d"]
                            )
                          )
                        : null}
                    </tbody>
                  </Table>
                </div>
              </Col>
            </div>
          </Card>
        </CardContainer>
      </div>
    );
  }

  function renderPoolSwapSize() {
    function renderRow(dateLabel, swapSize, change) {
      return (
        <tr>
          <th scope="row">{dateLabel}</th>
          {swapSize === -1 ? (
            <td colSpan={2} className="centered">
              <Link to="/membership">[go pro]</Link>
            </td>
          ) : (
            <>
              <td>${displayPrettyNumber(swapSize)} USD</td>
              <td>{change || "-"}</td>
            </>
          )}
        </tr>
      );
    }

    return (
      <div>
        <h6
          style={{
            fontFamily: "Nunito Sans, sans-serif",
            fontSize: 18,
            fontWeight: 900,
            color: "#042B4E",
          }}
        >
          Average swap size
        </h6>
        <p
          style={{
            fontFamily: "Nunito Sans, sans-serif",
            fontSize: 14,
            fontWeight: 900,
            color: "#042B4E",
            marginBottom: 12,
          }}
        >
          A growing average swap sizes over time is sign of an increase in{" "}
          <br />
          collected fees
        </p>
        <CardContainer>
          <Card style={{ borderRadius: 24 }}>
            <div className="card-block row">
              <Col sm="12" lg="12" xl="12">
                <div className="tableresponsive" style={{ overflowX: "auto" }}>
                  <Table>
                    <thead>
                      <tr>
                        <th scope="col"></th>
                        <th scope="col">Swap size</th>
                        <th scope="col">vs. Yesterday</th>
                      </tr>
                    </thead>
                    <tbody>
                      {poolInfo.pool_age_days >= 1
                        ? renderRow(
                            "Yesterday",
                            poolInfo.avg_swap_value_usd["1d"],
                            null
                          )
                        : null}
                      {poolInfo.pool_age_days >= 7
                        ? renderRow(
                            "Daily Avg Last 7 Days",
                            poolInfo.avg_swap_value_usd["7d"],
                            renderPctGainForPeriod(
                              poolInfo.avg_swap_value_usd["7d"],
                              poolInfo.avg_swap_value_usd["1d"]
                            )
                          )
                        : null}
                      {poolInfo.pool_age_days >= 14
                        ? renderRow(
                            "Daily Avg Last 14 Days",
                            poolInfo.avg_swap_value_usd["14d"],
                            renderPctGainForPeriod(
                              poolInfo.avg_swap_value_usd["14d"],
                              poolInfo.avg_swap_value_usd["1d"]
                            )
                          )
                        : null}
                      {poolInfo.pool_age_days >= 30
                        ? renderRow(
                            "Daily Avg Last 30 Days",
                            poolInfo.avg_swap_value_usd["30d"],
                            renderPctGainForPeriod(
                              poolInfo.avg_swap_value_usd["30d"],
                              poolInfo.avg_swap_value_usd["1d"]
                            )
                          )
                        : null}
                      {poolInfo.pool_age_days >= 30
                        ? renderRow(
                            "Daily Avg Since Inception",
                            poolInfo.avg_swap_value_usd["inception"],
                            renderPctGainForPeriod(
                              poolInfo.avg_swap_value_usd["inception"],
                              poolInfo.avg_swap_value_usd["1d"]
                            )
                          )
                        : null}
                    </tbody>
                  </Table>
                </div>
              </Col>
            </div>
          </Card>
        </CardContainer>
      </div>
    );
  }

  if (poolInfo && exchangePoolInfo) {
    return (
      <div>
        <CardContainer>
          <Card>
            <Col xl="12">
              <PoolTransactionsChart poolInfo={poolInfo} />
            </Col>
          </Card>
        </CardContainer>
        <Row>
          <Col xl="6">{renderPoolTransactions()}</Col>
          <Col xl="6">{renderPoolSwapSize()}</Col>
        </Row>
      </div>
    );
  }
  return null;
}
