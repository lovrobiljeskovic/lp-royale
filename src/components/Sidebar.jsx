import React from "react";
import { slide as Menu } from "react-burger-menu";
import { Link } from "react-router-dom";

export default (props) => {
  return (
    <Menu {...props}>
      <Link className="menu-item" to="/login">
        Login
      </Link>

      <Link className="menu-item" to="/about">
        About
      </Link>

      <Link className="menu-item" to="/faq">
        FAQ
      </Link>
    </Menu>
  );
};
