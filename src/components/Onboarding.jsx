import React, { useEffect, useState } from "react";
import "../styles.css";
import lproyale from "../assets/images/lp-royale-medium.svg";
import styled from "styled-components";
import rightArrow from "../assets/images/button-right.svg";
import coin from "../assets/images/coin-icon-2.svg";
import { useHistory } from "react-router-dom";
import pattern from "../assets/images/pattern.svg";
import liquidityPools from "../assets/images/liquidity-pools.svg";
import wallets from "../assets/images/wallets.svg";
import { StyledButton } from "../components/ButtonsAndInputs";

const OnboardingSteps = [
  {
    id: 0,
    title: "Get Started",
  },
  {
    id: 1,
    title: "Ethereum and wallets",
    content:
      "Ethereum is a blockchain network similar to Bitcoin. It allows open access to digital money and data-friendly services for everyone – regardless of location. It's a community-built technology behind the cryptocurrency ether (ETH) and thousands of decentralized applications available for use today. Ethereum wallets are applications that let you access your funds and interact with your Ethereum account.",
    contentTitle: "Let’s start with the basics",
  },
  {
    id: 2,
    title: "Liquidity Pools",
    content:
      "Simply put, a liquidity pool is a smart contract where you can deposit tokens (2 tokens, equal amount of each) used to facilitate decentralized trading, and lending of tokens. This is like market making in traditional financial markets with one key difference — liquidity pools are automated.",
    contentTitle: "What is a liquidity pool?",
  },
  {
    id: 3,
    title: "How pools work",
    content:
      "Liquidity pools are the backbone of many decentralized exchanges such as Uniswap. Users called liquidity providers (LP - that’s you!) add an equal value of two tokens in a pool. In exchange for providing their funds, they receive tokens representing their proportional share of the total liquidity.",
    contentTitle: "How does it work?",
  },
  {
    id: 4,
    title: "Risks and benefits",
    content:
      "To incentivize liquidity providers, a portion of the fees generated by transactions are redistributed proportionally to the value they provide - those are your gains. Because the tokens you provide to a liquidity pool has a ratio between the assets (ie. price) when you first started, any movement of that ratio will result in some Impermanent Losses, which is always a negative value. The higher the price divergence of the token prices in the pool, the more IL will be suffered.",
    contentTitle: "Why provide liquidity?",
  },
];

const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: relative;
  height: 104px;
  width: 100%;
  background: white;
  padding-left: 168px;
  padding-right: 152px;
  background: #f3f8fc;
  margin-bottom: 100px;
  justify-content: center;
  @media only screen and (max-width: 600px) {
    padding-left: 0px;
    padding-right: 0px;
  }
`;

const BackgroundContainer = styled.div`
  background: #f3f8fc;
  min-height: 100vh;
  > * {
    &:first-child {
      max-width: 1188px;
      padding-right: 24px;
      padding-left: 24px;
      margin-left: auto;
      margin-right: auto;
      @media only screen and (max-width: 600px) {
        padding-left: 1.5rem;
        padding-right: 1.5rem;
      }
    }
  }
`;

const Container = styled.div`
  display: flex;
  justify-content: center;
  @media only screen and (max-width: 600px) {
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
`;

const CardContainer = styled.div`
  background: white;
  width: 560px;
  min-height: 588px;
  border: 1px solid #9fbacf;
  box-sizing: border-box;
  box-shadow: 0px 4px 0px rgba(4, 43, 78, 0.14);
  border-radius: 24px;
  padding: 26px;
  h1 {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 900;
    font-size: 18px;
    color: #042b4e;
    margin-bottom: 16px;
  }
  p {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 22px;
    color: #042b4e;
  }
  @media only screen and (max-width: 600px) {
    width: 100%;
    min-height: fit-content;
  }
`;

const StepsContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 300px;
  align-items: flex-end;
  justify-content: space-between;
  margin-bottom: 52px;
  margin-right: 36px;
  @media only screen and (max-width: 600px) {
    width: 100%;
    align-items: center;
    margin-bottom: 20px;
    margin-right: 0px;
  }
`;

const Step = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 10px;
  margin-bottom: 10px;
`;

const P = styled.p`
  font-family: "MachineGunk";
  font-style: normal;
  font-weight: normal;
  color: ${(props) => (props.current ? "#5652CC" : "#9FBACF")};
  font-size: ${(props) => (props.current ? "28px" : "18px")};
  margin-bottom: 0px;
  &:hover {
    cursor: pointer;
  }
  @media only screen and (max-width: 600px) {
    font-size: 30px;
  }
`;

const Number = styled.div`
  font-family: "MachineGunk";
  font-size: 18px;
  color: white;
  -webkit-text-stroke: 0.5px #042b4e;
  text-shadow: 0px 1px 0px #000000;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 28px;
  height: 28px;
  background: linear-gradient(
    37.78deg,
    #5d45b3 -2.02%,
    #6b59d7 47.31%,
    #713ae7 47.32%,
    #49a8ff 103.26%
  );
  border: 1px solid #042b4e;
  box-sizing: border-box;
  border-radius: 50px;
  margin-right: 12px;
`;

const TopContainer = styled.div`
  min-height: 226px;
  background: url(${pattern}), #0fb1d8;
  overflow: hidden;
  img {
    position: ${(props) => (props.current === 1 ? "relative" : "absolute")};
    left: ${(props) => (props.current === 1 ? "" : "819px")};
    top: ${(props) => (props.current === 1 ? "" : "282px")};
  }
  @media only screen and (max-width: 600px) {
    img {
      position: relative;
      left: -25px !important;
      height: 200px;
      bottom: -62px;
      width: 300px;
      top: initial;
    }
  }
`;

const BottomContainer = styled.div`
  margin-top: 24px;
`;

const Steps = styled.div`
  display: flex;
  justify-content: flex-end;
  flex-direction: column;
`;

const ContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  @media only screen and (max-width: 600px) {
    width: 100%;
  }
`;

const Onboarding = (props) => {
  const [currentOnboardingStep, setOnboardingStep] = useState(
    OnboardingSteps[0]
  );
  const history = useHistory();

  const renderImage = (onboardingStepId) => {
    switch (onboardingStepId) {
      case 1: {
        return <img src={wallets} />;
      }
      case 2: {
        return <img src={liquidityPools} />;
      }
      default:
        return null;
    }
  };

  const handleClick = () => {
    setOnboardingStep(OnboardingSteps[currentOnboardingStep.id + 1]);
  };
  const handleGetStarted = () => {
    history.push("/dashboard");
  };
  return (
    <BackgroundContainer>
      <div>
        <HeaderContainer>
          <img src={lproyale} />
        </HeaderContainer>
        <Container>
          <StepsContainer>
            <Steps>
              {OnboardingSteps.map((onboardingStep, idx) => {
                return (
                  <Step key={onboardingStep.id}>
                    {currentOnboardingStep.id === onboardingStep.id ? (
                      <Number>{onboardingStep.id + 1}</Number>
                    ) : null}
                    <P
                      onClick={() => setOnboardingStep(OnboardingSteps[idx])}
                      current={currentOnboardingStep.id === onboardingStep.id}
                    >
                      {onboardingStep.title}
                    </P>
                  </Step>
                );
              })}
            </Steps>
            <img src={coin} />
          </StepsContainer>
          <ContentContainer>
            <CardContainer>
              {" "}
              <TopContainer current={currentOnboardingStep.id}>
                {currentOnboardingStep.id !== 0 &&
                  renderImage(currentOnboardingStep.id)}
              </TopContainer>
              <BottomContainer>
                <h1>
                  {currentOnboardingStep.id === 0
                    ? "Welcome, username"
                    : currentOnboardingStep.contentTitle}
                </h1>
                {currentOnboardingStep.id === 0 ? (
                  <div>
                    <p style={{ marginBottom: 0 }}>
                      Need to phrase content: something regarding what LP royale
                      is about, what the goal is, and what the rules are:
                    </p>
                    <p>
                      - Start with $100K
                      <br /> - Get the highest APY
                      <br /> - Timeline (6 weeks?)
                    </p>
                  </div>
                ) : (
                  currentOnboardingStep.content
                )}
              </BottomContainer>
            </CardContainer>
            <StyledButton
              primary
              size={currentOnboardingStep.id === 4 ? "xl" : "icon"}
              style={{ marginTop: 16 }}
              onClick={() =>
                currentOnboardingStep.id === 4
                  ? handleGetStarted()
                  : handleClick()
              }
            >
              {" "}
              {currentOnboardingStep.id === 4 ? (
                "Get Started"
              ) : (
                <img src={rightArrow} />
              )}
            </StyledButton>
          </ContentContainer>
        </Container>
      </div>
    </BackgroundContainer>
  );
};

export default Onboarding;
