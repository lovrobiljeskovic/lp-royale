import React, { useState, useEffect } from "react";
import axios from "axios";
import { Card, CardBody } from "reactstrap";
import styled from "styled-components";
import { isMobile } from "react-device-detect";
import { toDecimalPlaces } from "../helpers/util";
import leafCrownLeft from "../assets/images/leaf-crown-left.svg";
import leafCrownRight from "../assets/images/leaf-crown-right.svg";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  max-width: 1188px;
  margin-left: auto;
  margin-right: auto;
  padding-left: 24px;
  padding-right: 24px;
  margin-bottom: 520px;
  h1 {
    font-family: "MachineGunk";
    font-weight: 400;
    font-size: 28px;
    color: #042b4e;
  }
  @media only screen and (max-width: 600px) {
    padding-left: 1.5rem;
    padding-right: 1.5rem;
    margin-bottom: 120px;
  }
`;

const H2 = styled.h2`
  font-size: 24px;
  font-weight: 400;
  margin-bottom: 0px;
  text-shadow: 0px 1px 0px #042b4e;
  font-family: "MachineGunk";
  color: white;
  -webkit-text-stroke: 0.5px #042b4e;
`;

const H3 = styled.h3`
  font-size: 22px;
  font-weight: 800;
  color: white;
  margin-bottom: 0px !important;
  font-family: "Nunito Sans", sans-serif;
`;

const CardContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  margin-top: 50px;
  margin-bottom: 50px;

  @media only screen and (max-width: 600px) {
    flex-direction: column;
    margin-bottom: 0px;
  }
  @media only screen and (min-width: 600px) and (max-width: 1180px) {
    flex-wrap: wrap;
  }
  .card {
    width: 336px;
    height: 116px;
    border-radius: 24px;
    background: linear-gradient(
      37.78deg,
      #5d45b3 -2.02%,
      #6b59d7 47.31%,
      #713ae7 47.32%,
      #49a8ff 103.26%
    );
    border: 1px solid #042b4e;
    box-sizing: border-box;
    box-shadow: 0px 2px 0px rgba(4, 43, 78, 0.2),
      inset 0px -4px 0px rgba(4, 43, 78, 0.4);
    position: relative;
    @media only screen and (max-width: 600px) {
      width: 100%;
      margin-bottom: 40px !important;
    }
    @media only screen and (min-width: 600px) and (max-width: 1180px) {
      margin-bottom: 40px !important;
    }
    .card-body {
      display: flex;
      justify-content: space-between;
      align-items: center;
      padding: 20px;
      div {
        display: flex;
        flex-direction: column;
        align-items: flex-end;
        p {
          margin-bottom: 4px;
          font-size: 12px;
          font-weight: 800;
          font-family: "Nunito Sans", sans-serif;
          color: white;
        }
      }
    }
  }
  .second {
    margin-top: 20px;
    @media only screen and (max-width: 600px) {
      margin-top: 0px;
    }
  }
  .third {
    @media only screen and (min-width: 600px) and (max-width: 1180px) {
      margin-top: 0px !important;
    }
    margin-top: 40px;
    @media only screen and (max-width: 600px) {
      margin-top: 0px;
    }
  }
`;

const CircleDiv = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  width: 64px;
  height: 64px;
  background: linear-gradient(358.97deg, #0fb1d8 0.89%, #74c9dc 99.11%);
  border: 1px solid #042b4e;
  box-sizing: border-box;
  box-shadow: 0px 2px 0px rgba(4, 43, 78, 0.2),
    inset 0px -4px 0px rgba(4, 43, 78, 0.4);
  position: absolute;
  top: -32px;
  left: calc(50% - 32px);
  color: white;
  span {
    font-weight: bold;
    font-family: "MachineGunk";
    -webkit-text-stroke: 0.5px #042b4e;
    text-shadow: 0px 1px 0px #000000;
    font-size: 22px;
  }
`;

const ListContainer = styled.div`
  width: 60%;
  align-items: center;
  background: #ffffff;
  border: 1px solid #9fbacf;
  box-sizing: border-box;
  box-shadow: 0px 4px 0px rgba(4, 43, 78, 0.14);
  border-top-left-radius: 24px;
  border-top-right-radius: 24px;
  border-bottom-left-radius: ${(props) =>
    props.showMore === true ? "24px" : "0px"};
  border-bottom-right-radius: ${(props) =>
    props.showMore === true ? "24px" : "0px"};
  @media only screen and (max-width: 600px) {
    width: 100%;
  }
`;

const ListItem = styled.div`
  display: flex;
  align-items: center;
  padding-top: 14px;
  padding-bottom: 14px;
  border-bottom: 1px solid #9fbacf;
  box-sizing: border-box;
  &:last-child {
    border-bottom: none;
  }
  h3 {
    margin-left: auto;
    margin-right: 16px;
    font-size: 18px;
    font-weight: 800;
    font-family: "Nunito Sans", sans-serif;
    color: #5652cc;
    margin-bottom: 0px;
  }
  h2 {
    font-size: 18px;
    font-weight: 900;
    font-family: "Nunito Sans", sans-serif;
    color: #042b4e;
    margin-bottom: 0px;
  }
`;

const ListContent = styled.div`
  display: flex;
  flex-direction: column;
`;

const NumberDiv = styled.div`
  color: white;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 20px;
  width: 30px;
  height: 30px;
  margin-left: 16px;
  margin-right: 16px;
  background: linear-gradient(
    37.78deg,
    #5d45b3 -2.02%,
    #6b59d7 47.31%,
    #713ae7 47.32%,
    #49a8ff 103.26%
  );
  border: 1px solid #042b4e;
  box-sizing: border-box;
  box-shadow: 0px 2px 0px rgba(4, 43, 78, 0.2),
    inset 0px -4px 0px rgba(4, 43, 78, 0.4);
  border-radius: 8px;
  span {
    font-weight: bold;
  }
`;

const StyledButton = styled.button`
  align-items: center;
  padding-top: 14px;
  padding-bottom: 14px;
  border: 1px solid #9fbacf;
  box-sizing: border-box;
  width: 60%;
  border-bottom-left-radius: 24px;
  border-bottom-right-radius: 24px;
  font-size: 18px;
  font-weight: 900;
  font-family: "Nunito Sans", sans-serif;
  color: white;
  background: linear-gradient(
    37.78deg,
    #5d45b3 -2.02%,
    #6b59d7 47.31%,
    #713ae7 47.32%,
    #49a8ff 103.26%
  );
  @media only screen and (max-width: 600px) {
    width: 100%;
  }
`;

const Title = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 60%;
  @media only screen and (max-width: 600px) {
    flex-direction: column;
  }
`;

const FirstCardContainer = styled.div`
  margin-left: 30px;
  margin-right: 30px;
  @media only screen and (max-width: 600px) {
    margin-left: 0px;
    margin-right: 0px;
  }
`;

export const Leaderboard = () => {
  const [data, setData] = useState({ results: [] });
  const [showMore, setShowMore] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(
        "https://api.apy.vision/currentLPCount/0x6555c79a8829b793F332f1535B0eFB1fE4C11958/0x9e71bc8eea02a63969f509818f2dafb9254532904319f9dbda79b67bd34a5f3d/0x7084f5476618d8e60b11ef0d7d3f06914655adb8793e28ff7f018d4c76d505d5/11407437"
      );

      setData(result.data);
    };

    fetchData();
  }, []);

  const handleShowMore = () => {
    setShowMore(true);
  };

  const sorted = data?.results.sort((a, b) => (a.amount < b.amount ? 1 : -1));
  const theRest = sorted.slice(3, sorted.length);
  const numberOfItems = showMore ? theRest.length : 10;
  const renderFirst = () => {
    return sorted.slice(0, 1).map((item, idx) => {
      return (
        <FirstCardContainer>
          <Card key={idx}>
            <img
              style={{
                position: "absolute",
                bottom: 88,
                width: 40,
                height: 40,
                left: "calc(50% - 54px)",
                zIndex: 20,
              }}
              src={leafCrownLeft}
            />
            <CircleDiv>
              <span>1</span>
            </CircleDiv>
            <img
              style={{
                position: "absolute",
                bottom: 88,
                width: 40,
                height: 40,
                left: "calc(50% - (-12px))",
                zIndex: 20,
              }}
              src={leafCrownRight}
            />
            <CardBody>
              <H2>Username</H2>
              <div>
                <p> LP Gains:</p>
                <H3>{toDecimalPlaces(item.amount)}%</H3>
              </div>
            </CardBody>
          </Card>
        </FirstCardContainer>
      );
    });
  };

  const renderSecond = () => {
    return sorted.slice(1, 2).map((item, idx) => {
      return (
        <Card className="second" key={idx}>
          <CircleDiv>
            <span>2</span>
          </CircleDiv>
          <CardBody>
            <H2>Username</H2>
            <div>
              <p> LP Gains:</p>
              <H3>{toDecimalPlaces(item.amount)}%</H3>
            </div>
          </CardBody>
        </Card>
      );
    });
  };

  const renderThird = () => {
    return sorted.slice(2, 3).map((item, idx) => {
      return (
        <Card className="third" key={idx}>
          <CircleDiv>
            <span>3</span>
          </CircleDiv>
          <CardBody>
            <H2>Username</H2>
            <div>
              <p> LP Gains:</p>
              <H3>{toDecimalPlaces(item.amount)}%</H3>
            </div>
          </CardBody>
        </Card>
      );
    });
  };

  return (
    <Container>
      <h1>Current top 3!</h1>
      <CardContainer>
        {isMobile ? (
          <>
            {renderFirst()}
            {renderSecond()}
            {renderThird()}
          </>
        ) : (
          <>
            {renderSecond()}
            {renderFirst()}
            {renderThird()}
          </>
        )}
      </CardContainer>
      <Title>
        <h1>Leaderboard:</h1>
        <p
          style={{
            marginBottom: 0,
            fontSize: 16,
            color: "#9FBACF",
            fontWeight: 900,
          }}
        >
          8213 players
        </p>
      </Title>
      <ListContainer showMore={showMore}>
        {theRest.slice(0, numberOfItems).map((item, idx) => {
          return (
            <ListItem key={item.address}>
              {" "}
              <NumberDiv>
                <span>{idx + 4}</span>
              </NumberDiv>
              <ListContent>
                <h2>Username</h2>
              </ListContent>
              <h3>{toDecimalPlaces(item.amount)}%</h3>
            </ListItem>
          );
        })}
      </ListContainer>
      {!showMore && (
        <StyledButton onClick={() => handleShowMore()}>Show more</StyledButton>
      )}
    </Container>
  );
};
