import React from "react";
import { Card, CardHeader, Col, Row, Table } from "reactstrap";
import {
  displayPrettyNumber,
  getPoolIcon,
  toDecimalPlaces,
} from "../helpers/util";
import PctGainLossSpan from "./PctGainLossSpan";
import PoolReservesChart from "./PoolReservesChart";
import { Link } from "react-router-dom";
import { isMobile } from "react-device-detect";
import styled from "styled-components";

const CardContainer = styled.div`
  .card {
    margin-bottom: 60px;
    background: #ffffff;
    border: 1px solid #9fbacf;
    box-sizing: border-box;
    box-shadow: 0px 4px 0px rgba(4, 43, 78, 0.14);
    border-radius: 0px 0px 24px 24px;
  }
  .table {
    margin-bottom: 0px !important;
  }
  .table td {
    white-space: nowrap;
    border-top: 1px solid #9fbacf;
    font-size: 14px;
    text-align: center;
    font-family: "Nunito Sans", sans-serif;
    font-weight: 900;
    word-wrap: break-word;
    color: #1b3155;
    padding-top: 20px !important;
    padding-bottom: 20px !important;
    padding-left: 20px !important;
    padding-right: 20px !important;
    text-align: center;
    vertical-align: middle;
  }
  .table thead th {
    border-bottom: 1px solid #9fbacf;
    font-size: 12px;
    max-width: 180px;
    text-align: center;
    text-transform: uppercase;
    color: #1b3155;
    font-family: "Nunito Sans", sans-serif;
  }
  .table th {
    color: #1b3155;
    white-space: nowrap;
    border-top: 1px solid #9fbacf;
    padding-top: 20px !important;
    padding-bottom: 20px !important;
    padding-left: 20px !important;
    padding-right: 20px !important;
  }
`;

export default function (props) {
  const poolInfo = props.poolInfo;
  const exchangePoolInfo = props.exchangePoolInfo;

  function renderPctGainForPeriod(prev, curr) {
    const pct = ((parseFloat(curr) - prev) / prev) * 100;
    return <PctGainLossSpan val={pct}> %</PctGainLossSpan>;
  }

  function renderPoolReserves() {
    function renderRow(dateLabel, reserves, change) {
      return (
        <tr>
          <th scope="row">{dateLabel}</th>
          {reserves === -1 ? (
            <td colSpan={2} className="centered">
              <Link to="/membership">[GO VISION2K]</Link>
            </td>
          ) : (
            <>
              <td>${displayPrettyNumber(reserves)} USD</td>
              <td>{change || "-"}</td>
            </>
          )}
        </tr>
      );
    }

    return (
      <div>
        <h6
          style={{
            fontFamily: "Nunito Sans, sans-serif",
            fontSize: 18,
            fontWeight: 900,
            color: "#042B4E",
          }}
        >
          Pool Reserves
        </h6>
        <p
          style={{
            fontFamily: "Nunito Sans, sans-serif",
            fontSize: 14,
            fontWeight: 900,
            color: "#042B4E",
            marginBottom: 12,
          }}
        >
          This is the total reserves (in USD) of the pool. Higher reserves mean
          lower splippage from big buy/sell orders.
        </p>
        <CardContainer>
          <Card style={{ borderRadius: 24 }}>
            <div className="card-block row">
              <Col sm="12" lg="12" xl="12">
                <div className="tableresponsive" style={{ overflowX: "auto" }}>
                  <Table>
                    <thead>
                      <tr>
                        <th scope="col"></th>
                        <th scope="col">Reserves</th>
                        <th scope="col">vs. Yesterday</th>
                      </tr>
                    </thead>
                    <tbody>
                      {renderRow(
                        "Yesterday",
                        poolInfo.average_reserves["1d"],
                        null
                      )}
                      {poolInfo.pool_age_days >= 7
                        ? renderRow(
                            "Daily Avg Last 7 Day",
                            poolInfo.average_reserves["7d"],
                            renderPctGainForPeriod(
                              poolInfo.average_reserves["7d"],
                              poolInfo.average_reserves["1d"]
                            )
                          )
                        : null}
                      {poolInfo.pool_age_days >= 14
                        ? renderRow(
                            "Daily Avg Past 14 Days",
                            poolInfo.average_reserves["14d"],
                            renderPctGainForPeriod(
                              poolInfo.average_reserves["14d"],
                              poolInfo.average_reserves["1d"]
                            )
                          )
                        : null}
                      {poolInfo.pool_age_days >= 30
                        ? renderRow(
                            "Daily Avg Past 30 Days",
                            poolInfo.average_reserves["30d"],
                            renderPctGainForPeriod(
                              poolInfo.average_reserves["30d"],
                              poolInfo.average_reserves["1d"]
                            )
                          )
                        : null}
                      {poolInfo.pool_age_days >= 30
                        ? renderRow(
                            "Daily Avg Since Inception",
                            poolInfo.average_reserves["inception"],
                            renderPctGainForPeriod(
                              poolInfo.average_reserves["inception"],
                              poolInfo.average_reserves["1d"]
                            )
                          )
                        : null}
                    </tbody>
                  </Table>
                </div>
              </Col>
            </div>
          </Card>
        </CardContainer>
      </div>
    );
  }

  function renderPoolVols() {
    function renderRow(dateLabel, vols, change) {
      return (
        <tr>
          <th scope="row">{dateLabel}</th>
          {vols === -1 ? (
            <td colSpan={2} className="centered">
              <Link to="/membership">[GO VISION2K]</Link>
            </td>
          ) : (
            <>
              <td>${displayPrettyNumber(vols)} USD</td>
              <td>{change || "-"}</td>
            </>
          )}
        </tr>
      );
    }

    return (
      <div>
        <h6
          style={{
            fontFamily: "Nunito Sans, sans-serif",
            fontSize: 18,
            fontWeight: 900,
            color: "#042B4E",
          }}
        >
          Pool volume
        </h6>
        <p
          style={{
            fontFamily: "Nunito Sans, sans-serif",
            fontSize: 14,
            fontWeight: 900,
            color: "#042B4E",
            marginBottom: 12,
          }}
        >
          This is the volume history of the pool. This determines how much in
          fees Liquidity Providers collect.
        </p>
        <CardContainer>
          <Card style={{ borderRadius: 24 }}>
            <div className="card-block row">
              <Col sm="12" lg="12" xl="12">
                <div className="tableresponsive" style={{ overflowX: "auto" }}>
                  <Table>
                    <thead>
                      <tr>
                        <th scope="col"></th>
                        <th scope="col">Volume</th>
                        <th scope="col">vs. Yesterday</th>
                      </tr>
                    </thead>
                    <tbody>
                      {poolInfo.pool_age_days >= 1
                        ? renderRow(
                            "Yesterday",
                            poolInfo.average_volumes["1d"],
                            null
                          )
                        : null}
                      {poolInfo.pool_age_days >= 7
                        ? renderRow(
                            "Daily Avg Last 7 Days",
                            poolInfo.average_volumes["7d"],
                            renderPctGainForPeriod(
                              poolInfo.average_volumes["7d"],
                              poolInfo.average_volumes["1d"]
                            )
                          )
                        : null}
                      {poolInfo.pool_age_days >= 14
                        ? renderRow(
                            "Daily Avg Last 14 Days",
                            poolInfo.average_volumes["14d"],
                            renderPctGainForPeriod(
                              poolInfo.average_volumes["14d"],
                              poolInfo.average_volumes["1d"]
                            )
                          )
                        : null}
                      {poolInfo.pool_age_days >= 30
                        ? renderRow(
                            "Daily Avg Last 30 Days",
                            poolInfo.average_volumes["30d"],
                            renderPctGainForPeriod(
                              poolInfo.average_volumes["30d"],
                              poolInfo.average_volumes["1d"]
                            )
                          )
                        : null}
                      {poolInfo.pool_age_days >= 30
                        ? renderRow(
                            "Daily Avg Since Inception",
                            poolInfo.average_volumes["inception"],
                            renderPctGainForPeriod(
                              poolInfo.average_volumes["inception"],
                              poolInfo.average_volumes["1d"]
                            )
                          )
                        : null}
                    </tbody>
                  </Table>
                </div>
              </Col>
            </div>
          </Card>
        </CardContainer>
      </div>
    );
  }

  if (poolInfo && exchangePoolInfo) {
    return (
      <div>
        <CardContainer>
          <Card>
            <div className="card-block row">
              <Col sm="12" lg="12" xl="12">
                <PoolReservesChart
                  market={poolInfo.pool_provider_name}
                  poolAddrs={poolInfo.pool_address}
                />
              </Col>
            </div>
          </Card>
        </CardContainer>
        <Row>
          <Col xl="6">{renderPoolReserves()}</Col>
          <Col xl="6">{renderPoolVols()}</Col>
        </Row>
      </div>
    );
  }
  return null;
}
