import React from 'react';

export const MENUITEMS = [
  {
    title: 'Home',
    icon: <i className="pe-7s-home pe-lg"></i>,
    path: `/`,
    type: 'sub',
    active: true,
    bookmark: true,
    children: [],
  },
  {
    title: 'LP Royale',
    icon: <i className="pe-7s-cup"></i>,
    type: 'sub',
    path: '/dashboard',
    active: false,
    children: [],
  },
  {
    title: 'All Pools',
    icon: <i className="pe-7s-graph3"></i>,
    type: 'sub',
    path: '/all-pools',
    active: false,
    children: [],
  },
  {
    title: 'About / FAQs',
    icon: <i className="pe-7s-more"></i>,
    type: 'sub',
    path: '/all-pools',
    active: false,
    children: [],
  },
  {
    title: 'Pro',
    icon: <i className="pe-7s-portfolio pe-lg"></i>,
    type: 'sub',
    path: `/membership`,
    active: false,
    bookmark: true,
    children: [],
  },
];
