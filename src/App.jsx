import React from "react";
import "./App.css";
import { Route, Switch } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";

import { toast, ToastContainer } from "react-toastify";
import { Home } from "./components/Home";
import Header from "./components/Header";
import { SignUp } from "./components/SignUp";
import styled from "styled-components";
import { Leaderboard } from "./components/Leaderboard";
import { LogIn } from "./components/LogIn";
import coin from "./assets/images/coin-icon-2.svg";
import { Footer } from "./components/Footer";
import Onboarding from "./components/Onboarding";
import { Dashboard } from "./components/Dashboard";
import LoggedInHeader from "./components/LoggedInHeader";
import { LoggedInFooter } from "./components/LoggedInFooter";
import { AllPools } from "./components/AllPools";
import { LoggedInLeaderboard } from "./components/LoggedInLeaderboard";
import { Stats } from "./components/Stats";
import { MoreInfo } from "./components/MoreInfo";

const PrizeContainer = styled.div`
  display: flex;
  position: relative;
  justify-content: center;
  align-items: center;
  height: 338px;
  background: #5652cc;
  margin-top: 45px;
  margin-bottom: 90px;
  p {
    font-size: 22px;
    font-weight: 700;
    color: white;
  }
  @media only screen and (max-width: 600px) {
    margin-top: 200px;
    height: 180px;
  }
`;

const App = () => {
  return (
    <div>
      <Switch>
        <Route exact path="/">
          <Header />
          <Home />
          <PrizeContainer>
            <p>Prizes area</p>
            <img style={{ position: "absolute", bottom: -35 }} src={coin} />
          </PrizeContainer>
          <Leaderboard />
          <Footer />
        </Route>
        <Route path="/signup">
          <SignUp />
        </Route>
        <Route path="/login">
          <LogIn />
        </Route>
        <Route path="/onboarding">
          <Onboarding />
        </Route>
        <Route path="/dashboard">
          <div
            style={{
              minHeight: "100vh",
              margin: 0,
              position: "relative",
              backgroundColor: "#F3F8FC",
            }}
          >
            <LoggedInHeader />
            <Dashboard />
            <LoggedInFooter />
          </div>
        </Route>
        <Route path="/all-pools">
          <div
            style={{
              minHeight: "100vh",
              margin: 0,
              position: "relative",
              backgroundColor: "#F3F8FC",
            }}
          >
            <LoggedInHeader />
            <AllPools />
            <LoggedInFooter />
          </div>
        </Route>
        <Route path="/leaderboard">
          <div
            style={{
              minHeight: "100vh",
              margin: 0,
              position: "relative",
              backgroundColor: "#F3F8FC",
            }}
          >
            <LoggedInHeader />
            <LoggedInLeaderboard />
            <LoggedInFooter />
          </div>
        </Route>
        <Route path="/stats">
          <LoggedInHeader />
          <Stats />
        </Route>
        <Route path="/more-info">
          <LoggedInHeader />
          <MoreInfo />
        </Route>
      </Switch>
      <ToastContainer />
    </div>
  );
};

export default App;
