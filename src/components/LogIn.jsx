import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { Input, Card, CardBody, Label, FormGroup } from "reactstrap";
import lproyale from "../assets/images/lp-royale-medium.svg";
import pattern from "../assets/images/pattern.svg";
import { StyledButton, StyledInput } from "../components/ButtonsAndInputs";

const Container = styled.div`
  background: url(${pattern}), #0fb1d8;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;
  min-height: 100vh;
  .card {
    min-width: 464px;
    border-radius: 24px;
    @media only screen and (max-width: 600px) {
      min-width: 0px;
    }
  }
  img {
    position: absolute;
    top: -50px;
    left: 126px;
    @media only screen and (max-width: 600px) {
      left: 60px;
    }
  }
  span {
    margin-right: 16px;
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 900;
    font-size: 14px;
    color: #9fbacf !important;
  }
  h2 {
    font-family: "MachineGunk";
    font-style: normal;
    font-weight: normal;
    font-size: 28px;
    text-align: start;
    padding-top: 56px;
    padding-bottom: 12px;
  }
`;

const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: start;
  p {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 900;
    font-size: 14px;
    color: #9fbacf !important;
  }
`;

export const LogIn = () => {
  return (
    <Container>
      <Card>
        <img src={lproyale} />
        <CardBody>
          <h2>Welcome back!</h2>
          <InputContainer>
            <StyledInput
              type="text"
              placeholder="Your email"
              style={{ width: "100%" }}
            />
          </InputContainer>
          <div
            className="d-flex"
            style={{
              alignItems: "center",
              justifyContent: "space-between",
              marginTop: 16,
              marginBottom: 16,
            }}
          >
            <span>We’ll send you a link to login</span>
            <StyledButton primary size="xl">
              Login
            </StyledButton>
          </div>
        </CardBody>
      </Card>
    </Container>
  );
};
