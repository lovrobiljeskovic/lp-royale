import React, { useEffect, useState } from "react";
import styled from "styled-components";
import axios from "axios";
import DoubleTokenLogo from "./DoubleTokenLogo";
import { StyledButton } from "./ButtonsAndInputs";
import {
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Container,
} from "reactstrap";
import APYSection from "./APYSection";
import { getUniswapPairData, graphPricesUniswap } from "./uniswap/PairData";
import { UNISWAP_INFO_API_URL } from "../constants";
import { toast } from "react-toastify";
import { fetchEthPriceFor } from "../helpers/util";
import { isMobile } from "react-device-detect";
import PoolReservesSection from "./PoolReservesSection";
import PoolHodlStatsSection from "./PoolHodlStatsSection";
import PoolTransactionSection from "./PoolTransactionSection";

const StyledContainer = styled(Container)`
  margin-top: 40px;
  .nav {
    .nav-tabs {
      border-bottom: none;
    }
    .nav-link {
      border-radius: 16px 16px 0px 0px;
      font-family: "Nunito Sans", sans-serif;
      font-weight: 900;
      font-size: 14px;
      line-height: 19px;
      color: #042b4e;
      text-transform: uppercase;
    }
    .active {
      background: #c7d8e9;
      color: white;
      -webkit-text-stroke: 0.5px #042b4e;
      text-shadow: 0px 1px 0px #000000;
    }
  }
  h1 {
    color: #042b4e;
    font-family: "MachineGunk";
    font-style: normal;
    font-weight: normal;
    font-size: 32px;
  }
`;

const HeaderContainer = styled.div`
  display: flex;
  background: #ffffff;
  border: 1px solid #9fbacf;
  box-sizing: border-box;
  box-shadow: 0px 4px 0px rgba(4, 43, 78, 0.14);
  border-radius: 24px;
  padding: 24px;
  margin-bottom: 64px;
  @media only screen and (max-width: 600px) {
    flex-direction: column;
  }
  @media only screen and (min-width: 600px) and (max-width: 1028px) {
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
`;

const NavContainer = styled.div`
  display: flex;
  .nav-item {
    cursor: pointer;
  }
  @media only screen and (max-width: 600px) {
    width: 100%;
    margin-bottom: 26px;
    .nav {
      width: 100%;
      flex-direction: column;
      margin-left: 0px;
      text-align: center;
    }
  }
`;

const RightContainer = styled.div`
  width: 40%;
  @media only screen and (min-width: 600px) and (max-width: 1028px) {
    width: auto;
  }
  @media only screen and (max-width: 600px) {
    width: 100%;
    justify-content: center;
  }
`;

const LeftContainer = styled.div`
  @media only screen and (min-width: 600px) and (max-width: 1028px) {
    margin-bottom: 20px;
  }
  @media only screen and (max-width: 600px) {
    margin-bottom: 20px;
  }
`;

export const Stats = (props) => {
  const [poolInfo, setPoolInfo] = useState();
  const [currentPrices, setCurrentPrices] = useState();
  const [poolData, setPoolData] = useState();
  const [ethPrice, setEthPrice] = useState();
  const [market, setMarket] = useState();
  const [exchangePoolInfo, setExchangePoolInfo] = useState();
  const [tab, setTab] = useState("1");
  const poolAddrs = "0x51c1d5cdf10dda49219054920c22c8d4a23eed89";
  useEffect(() => {
    const fetchData = async () => {
      const response = await axios.get(
        `${UNISWAP_INFO_API_URL}/api/v1/pools/${poolAddrs}`
      );
      if (response.data) {
        setPoolInfo(response.data);
        setMarket(response.data.pool_provider_name);
      } else {
        toast.error(
          `Whoops - unable to find pool with address ${poolAddrs} - please reach out to an admin on our Discord channel.`,
          {
            position: toast.POSITION.TOP_CENTER,
            autoClose: 6000,
          }
        );
      }
    };

    fetchData();
  }, [poolAddrs]);

  useEffect(() => {
    async function checkForEthPrice() {
      if (!ethPrice && market) {
        let [newPrice, oneDayPrice, priceChange] = await fetchEthPriceFor(
          market
        );
        setEthPrice(newPrice);
      }
    }

    checkForEthPrice();
  }, [market]);

  useEffect(() => {
    async function fetchData() {
      let data = null;
      if (!poolData) {
        data = await getUniswapPairData(poolAddrs);
        setPoolData(data);
      }
    }

    if (!poolData && ethPrice) {
      fetchData();
    }
  }, [ethPrice, market]);

  async function fallbackPrices(prices) {
    let token0Price = parseFloat(prices[0]);
    let token1Price = parseFloat(prices[1]);
    let result = [];
    if (market === "uniswap") {
      result = await graphPricesUniswap(poolAddrs);
    }

    if (result && token0Price === 0) {
      return [token1Price * parseFloat(result[1]), token1Price];
    } else if (result && token1Price === 0) {
      return [token0Price, token0Price * parseFloat(result[0])];
    }
    return prices;
  }

  useEffect(() => {
    async function fetchPoolPrices() {
      let prices = [];
      if (market === "balancer") {
        prices = poolData.map((token) => parseFloat(token.price));
      } else {
        // uniswap, sushiswap, oneinch
        prices = [
          poolData.token0.derivedETH * ethPrice,
          poolData.token1.derivedETH * ethPrice,
        ];
        prices =
          prices[0] === 0 || prices[1] === 0
            ? await fallbackPrices(prices)
            : prices;
      }
      setCurrentPrices(prices);
      setExchangePoolInfo(poolData);
    }

    if (ethPrice && poolData) {
      fetchPoolPrices();
    }
  }, [ethPrice, poolData]);

  return (
    <StyledContainer>
      <h1>Pool Statistics</h1>
      <HeaderContainer>
        <LeftContainer
          className={
            isMobile ? "d-flex flex-column align-items-center" : "d-flex"
          }
          style={{ flex: 1 }}
        >
          <div style={{ marginRight: 20 }}>
            <DoubleTokenLogo
              size={52}
              style={
                isMobile
                  ? { position: "absolute", right: "20px" }
                  : { right: "0px" }
              }
            />
          </div>
          <div
            className="d-flex flex-column align-items-center"
            style={isMobile ? { marginLeft: 0 } : { marginLeft: 48 }}
          >
            <p
              style={{
                marginBottom: 0,
                fontSize: 24,
                fontWeight: 900,
                fontFamily: "Nunito Sans, sans-serif",
                color: "#042B4E",
              }}
            >
              LINK{" "}
              <span
                style={{
                  marginBottom: 0,
                  fontSize: 16,
                  fontWeight: 900,
                  fontFamily: "Nunito Sans, sans-serif",
                  color: "#042B4E",
                }}
              >
                $12.54
              </span>{" "}
              / ETH{" "}
              <span
                style={{
                  marginBottom: 0,
                  fontSize: 16,
                  fontWeight: 900,
                  fontFamily: "Nunito Sans, sans-serif",
                  color: "#042B4E",
                }}
              >
                $574.07
              </span>
            </p>
            <p
              style={{
                marginBottom: 0,
                fontSize: 14,
                fontWeight: 900,
                fontFamily: "Nunito Sans, sans-serif",
                color: "#042B4E",
              }}
            >
              Uniswap • 204 days old
            </p>
          </div>
        </LeftContainer>
        <RightContainer className="d-flex align-items-center justify-content-between">
          <div className="flex-column" style={{ marginRight: 12 }}>
            <p
              style={{
                marginBottom: 0,
                fontWeight: "bold",
                fontSize: 14,
                fontFamily: "Nunito Sans, sans-serif",
                color: "#042B4E",
              }}
            >
              Total Reserve
            </p>
            <p
              style={{
                marginBottom: 0,
                fontWeight: 900,
                fontSize: 18,
                fontFamily: "Nunito Sans, sans-serif",
                color: "#042B4E",
              }}
            >
              $28.4M
            </p>
          </div>
          <div className="flex-column" style={{ marginRight: 12 }}>
            <p
              style={{
                marginBottom: 0,
                fontWeight: "bold",
                fontSize: 14,
                fontFamily: "Nunito Sans, sans-serif",
                color: "#042B4E",
              }}
            >
              Volume (24h)
            </p>
            <p
              style={{
                marginBottom: 0,
                fontWeight: 900,
                fontSize: 18,
                fontFamily: "Nunito Sans, sans-serif",
                color: "#042B4E",
              }}
            >
              $2.93M
            </p>
          </div>
          <StyledButton primary size="lg" style={{ whiteSpace: "nowrap" }}>
            Add liquidity
          </StyledButton>
        </RightContainer>
      </HeaderContainer>
      <NavContainer>
        <Nav className="nav-primary" tabs>
          <NavItem>
            <NavLink
              className={tab === "1" ? "active" : ""}
              onClick={() => {
                setTab("1");
              }}
            >
              <i className="icofont icofont-ui-home" />
              APY vs. Impermanent Loss
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={tab === "2" ? "active" : ""}
              onClick={() => {
                setTab("2");
              }}
            >
              <i className="icofont icofont-listing-box" />
              Reserve/volume
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={tab === "3" ? "active" : ""}
              onClick={() => {
                setTab("3");
              }}
            >
              <i className="icofont icofont-listing-box" />
              Assets growth
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={tab === "4" ? "active" : ""}
              onClick={() => {
                setTab("4");
              }}
            >
              <i className="icofont icofont-listing-box" />
              Transactions
            </NavLink>
          </NavItem>
        </Nav>
      </NavContainer>
      <TabContent
        activeTab={tab}
        style={{ width: "100%", marginBottom: "60px" }}
      >
        <TabPane tabId="1">
          <APYSection poolInfo={poolInfo} currentPrices={currentPrices} />
        </TabPane>
        <TabPane tabId="2">
          <PoolReservesSection
            poolInfo={poolInfo}
            currentPrices={currentPrices}
            exchangePoolInfo={exchangePoolInfo}
            market={poolInfo && poolInfo.pool_provider_name}
          />
        </TabPane>
        <TabPane tabId="3">
          <PoolHodlStatsSection
            poolInfo={poolInfo}
            currentPrices={currentPrices}
          />
        </TabPane>
        <TabPane tabId="4">
          <PoolTransactionSection
            poolInfo={poolInfo}
            exchangePoolInfo={exchangePoolInfo}
          />
        </TabPane>
      </TabContent>
    </StyledContainer>
  );
};
