import React from "react";
import styled from "styled-components";
import { Button, Card, CardBody, Input } from "reactstrap";
import coin from "../assets/images/coin-icon.svg";
import pattern from "../assets/images/pattern.svg";
import { StyledButton, StyledInput } from "../components/ButtonsAndInputs";

const BackgroundContainer = styled.div`
  height: 1018px;
  .topHalf {
    background: url(${pattern}), #0fb1d8;
    height: 480px;
    width: 100%;
  }
  .bottomHalf {
    background-color: #f3f8fc;
    height: 538px;
    width: 100%;
    position: absolute;
    top: 584px;
    z-index: -1;
  }
`;

const Container = styled.div`
  background: none !important;
  max-width: 1188px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin-left: auto;
  margin-right: auto;
  background: white;
  padding-left: 24px;
  padding-right: 24px;
  position: relative;
  @media only screen and (max-width: 600px) {
    padding-left: 1.5rem;
    padding-right: 1.5rem;
    margin-right: 0px;
    margin-left: 0px;
  }
`;

const H1 = styled.h1`
  font-weight: 400;
  font-size: 62px !important;
  text-align: start;
  margin-bottom: 16px !important;
  font-family: "MachineGunk";
  text-shadow: 0px 2px 0px #042b4e;
  color: white;
  -webkit-text-stroke: 0.5px #042b4e;
  @media only screen and (max-width: 600px) {
    font-size: 2.5rem !important;
  }
  @media only screen and (min-width: 600px) and (max-width: 1180px) {
    text-align: center;
  }
`;

const P = styled.p`
  font-weight: 800;
  font-size: 20px;
  text-align: start;
  line-height: 24.55px;
  font-family: "Nunito Sans", sans-serif;
  color: white;
  @media only screen and (min-width: 600px) and (max-width: 1180px) {
    text-align: center;
  }
`;

const CardContainer = styled.div`
  @media only screen and (min-width: 600px) and (max-width: 1180px) {
    justify-content: center;
  }
  > :first-child {
    background: linear-gradient(
      37.78deg,
      #5d45b3 -2.02%,
      #6b59d7 47.31%,
      #713ae7 47.32%,
      #49a8ff 103.26%
    );

    border: 1px solid #042b4e;
    box-sizing: border-box;
    box-shadow: 0px 2px 0px rgba(4, 43, 78, 0.2),
      inset 0px -4px 0px rgba(4, 43, 78, 0.4);
    border-radius: 24px;
    width: 464px;
    margin-top: 34px;
    margin-right: 34px;
    border-radius: 16px !important;
    .card-body {
      padding: 32px;
    }
    @media only screen and (max-width: 600px) {
      margin: 40px 0px 0px 0px;
    }
    @media only screen and (min-width: 600px) and (max-width: 1180px) {
      margin-right: 0px;
    }
  }
  width: 100%;
  display: flex;
  p {
    color: white;
    font-weight: 400;
    font-size: 24px !important;
    margin-bottom: 0.5rem;
    font-family: "MachineGunk";
    text-shadow: 0px 2px 0px #042b4e;
  }
  span {
    color: white;
    font-size: 14px !important;
    font-family: "Nunito Sans", sans-serif;
    font-weight: 800;
  }
  a {
    text-decoration: underline !important;
  }
  @media only screen and (max-width: 600px) {
    p {
      margin-left: 0px;
      text-align: center;
    }
    span {
      margin-left: 0px;
    }
  }
`;

const ContentContainer = styled.div`
  display: flex;
  margin-bottom: 12px;
  flex-direction: column;
  input {
    margin-right: 20px;
    margin-bottom: 20px;
    font-family: "Nunito Sans", sans-serif;
    font-weight: 800;
    font-size: 16px;
    color: #9fbacf;
  }
  select {
    margin-right: 20px;
  }
  button {
    border-radius: 5px;
    white-space: nowrap;
  }
  @media only screen and (max-width: 600px) {
    margin-left: 0px;
    margin-right: 0px;
    flex-direction: column;
    .form-control {
      width: 100% !important;
      margin-bottom: 1rem;
    }
  }
`;

const GridContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  margin-top: 200px;
  margin-bottom: 80px;
  @media only screen and (min-width: 600px) and (max-width: 1180px) {
    display: grid;
    grid-template-columns: 1fr 1fr;
    justify-items: center;
    margin-bottom: 0px;
    margin-top: 94px;
  }
  > *:not(:last-child) {
    margin-right: 54px;
    @media only screen and (min-width: 600px) and (max-width: 1180px) {
      margin-right: 0px;
    }
  }
  h1 {
    font-size: 28px !important;
    color: #042b4e;
    margin-bottom: 36px !important;
  }
  @media only screen and (max-width: 600px) {
    flex-direction: column;
    margin-top: 60px;
    margin-bottom: 0px;
  }
`;

const InfoContainer = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  justify-content: center;
  align-items: flex-start;
  bottom: ${(props) => props.bottom};
  top: ${(props) => props.top};
  h2 {
    font-weight: 400;
    padding: 8px 16px 10px 40px;
    background: #042b4e;
    border-radius: 8px;
    font-family: "MachineGunk";
    color: white;
    text-align: center;
    font-size: 32px;
    max-height: 52px;
    margin-bottom: 16px;
    margin-left: 32px;
    @media only screen and (min-width: 600px) and (max-width: 1180px) {
      font-size: 22px;
    }
  }
  p {
    width: 230px;
    font-size: 16px;
    font-family: "Nunito Sans", sans-serif;
    font-weight: 800;
    color: #042b4e;
    @media only screen and (min-width: 600px) and (max-width: 1180px) {
      width: 200px;
      font-size: 14px;
    }
  }

  @media only screen and (min-width: 600px) and (max-width: 1180px) {
    top: 0px !important;
    bottom: 0px !important;
    align-self: flex-start;
  }

  @media only screen and (max-width: 600px) {
    bottom: 0px;
    top: 0px;
    h2 {
      margin-left: 14px;
      font-size: 16px;
      margin-bottom: 12px;
    }
    p {
      font-size: 16px;
    }
  }
`;

const CircleDiv = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  width: 64px;
  height: 64px;
  background: linear-gradient(180deg, #ffdf28 0%, #ebce3a 100%);
  position: absolute;
  top: ${(props) => props.top};
  left: ${(props) => props.left};
  color: white;
  border: 1px solid #042b4e;
  box-sizing: border-box;
  box-shadow: 0px 2px 0px rgba(4, 43, 78, 0.2),
    inset 0px -4px 0px rgba(4, 43, 78, 0.4);
  @media only screen and (max-width: 600px) {
    width: 48px;
    height: 48px;
  }
  @media only screen and (min-width: 600px) and (max-width: 1180px) {
    top: -7px;
    bottom: 2px;
  }
`;

const Img = styled.img`
  width: 490px;
  height: 395px;
  position: absolute;
  left: 674px;
  top: 180px;
  @media only screen and (max-width: 1180px) {
    display: none;
  }
  @media only screen and (max-width: 600px) {
    display: none;
  }
`;

const LeftPanel = styled.div`
  display: flex;
  flex-direction: column;
  width: 50%;
  margin-top: 26px;
  @media only screen and (max-width: 1180px) {
    width: 100%;
    align-items: center;
  }
  @media only screen and (max-width: 600px) {
    width: 100%;
  }
`;

export const Home = (props) => {
  return (
    <BackgroundContainer>
      <div className="topHalf">
        <Container>
          <div
            className="d-flex"
            style={{
              width: "100%",
            }}
          >
            <LeftPanel>
              <div className="mt-4">
                <H1>
                  Try liquidity providing <br />
                  without the risks
                </H1>
                <P>
                  Learn how liquidity providing works, compete with <br />
                  others and level up your DeFi skills without investing <br />
                  real money
                </P>
              </div>
              <CardContainer>
                <Card>
                  <CardBody>
                    <p style={{ marginBottom: 16 }}>Get Started:</p>
                    <ContentContainer>
                      <StyledInput type="text" placeholder="Your email" />
                      <StyledInput type="text" placeholder="Your username" />
                      <div
                        className="d-flex"
                        style={{
                          alignItems: "center",
                          justifyContent: "space-between",
                        }}
                      >
                        <span>We’ll send you a link to login</span>
                        <StyledButton primary size="xl">
                          SignUp
                        </StyledButton>
                      </div>
                    </ContentContainer>
                  </CardBody>
                </Card>
              </CardContainer>
            </LeftPanel>
            <Img src={coin} />
          </div>
          <GridContainer>
            <InfoContainer bottom={"60px"}>
              <CircleDiv top="6px" left="-2px" />
              <h2>Learn</h2>
              <p>
                Master the basics and learn <br /> how liquidity providing works
              </p>
            </InfoContainer>
            <InfoContainer top={"20px"}>
              <CircleDiv top="6px" left="-2px" />
              <h2>Compete</h2>
              <p>
                Track your performance and <br />
                compete with other players
              </p>
            </InfoContainer>
            <InfoContainer bottom={"60px"}>
              <CircleDiv top="6px" left="-2px" />
              <h2>Win</h2>
              <p>Be the best and win badges and exciting prizes</p>
            </InfoContainer>
            <InfoContainer top={"20px"}>
              <CircleDiv top="-6px" left="-2px" />
              <h2>Risk-free</h2>
              <p>
                The safest way to try liquidity <br /> providing without
                investing real money
              </p>
            </InfoContainer>
          </GridContainer>
        </Container>
        <div className="bottomHalf"></div>
      </div>
    </BackgroundContainer>
  );
};
