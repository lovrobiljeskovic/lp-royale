import React, { useEffect, useState } from "react";
import { Card, CardHeader, Col, Table } from "reactstrap";
import { Link } from "react-router-dom";
import { displayPrettyNumber, toDecimalPlaces } from "../helpers/util";
import {
  Bar,
  BarChart,
  CartesianGrid,
  Legend,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";
import PctGainLossSpan from "./PctGainLossSpan";
import dayjs from "dayjs";
import styled from "styled-components";
import { isMobile } from "react-device-detect";

const BarContainer = styled.div`
  .recharts-cartesian-axis-tick {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 800;
    font-size: 12px;
    text {
      fill: #042b4e;
    }
  }
  .recharts-cartesian-axis-tick-value {
    fill: #042b4e;
  }
  .recharts-legend-wrapper {
    bottom: -30px !important;
  }
  .recharts-default-legend {
    text-align: center !important;
    @media only screen and (max-width: 600px) {
      text-align: end !important;
      margin-left: 0px !important;
    }
  }
`;

const CardContainer = styled.div`
  .card {
    margin-bottom: 60px;
    background: #ffffff;
    border: 1px solid #9fbacf;
    box-sizing: border-box;
    box-shadow: 0px 4px 0px rgba(4, 43, 78, 0.14);
    border-radius: 0px 0px 24px 24px;
    /* .tableresponsive {
      ::-webkit-scrollbar {
        height: 5px;
      }

      ::-webkit-scrollbar-track {
        box-shadow: inset 0 0 5px grey;
        border-radius: 10px;
      }

      ::-webkit-scrollbar-thumb {
        background: black;
        border-radius: 10px;
      }

      ::-webkit-scrollbar-thumb:hover {
        background: rgb(54, 56, 58);
      }
    } */
  }
  .table {
    margin-bottom: 0px !important;
  }
  .table td {
    white-space: nowrap;
    border-top: 1px solid #9fbacf;
    font-size: 14px;
    text-align: center;
    font-family: "Nunito Sans", sans-serif;
    font-weight: 900;
    word-wrap: break-word;
    color: #1b3155;
    padding-top: 20px !important;
    padding-bottom: 20px !important;
    padding-left: 20px !important;
    padding-right: 20px !important;
    text-align: center;
    vertical-align: middle;
  }
  .table thead th {
    border-bottom: 1px solid #9fbacf;
    font-size: 12px;
    max-width: 180px;
    text-align: center;
    text-transform: uppercase;
    color: #1b3155;
    font-family: "Nunito Sans", sans-serif;
  }
  .table th {
    white-space: nowrap;
    border-top: none;
    padding-top: 20px !important;
    padding-bottom: 20px !important;
    padding-left: 20px !important;
    padding-right: 20px !important;
  }
`;

export default function APYSection(props) {
  const [barChartData, setBarChartData] = useState();

  const poolInfo = props.poolInfo;
  const currentPrices = props.currentPrices;
  function convertToChartData(entry) {
    const results = [];

    if (poolInfo.pool_age_days >= 30) {
      results.push(
        {
          name: "Since Inception*",
          "Fees APY": entry.fee_apys["inception"].toFixed(2),
          "IL APY": entry.il_apys["average_usd_since_inception"].toFixed(2),
          "Net APY": (
            entry.fee_apys["inception"] -
            entry.il_apys["average_usd_since_inception"]
          ).toFixed(2),
        },
        {
          name: "Since Inception**",
          "Fees APY": entry.fee_apys["inception"].toFixed(2),
          "IL APY": entry.il_apys["inception"].toFixed(2),
          "Net APY": (
            entry.fee_apys["inception"] - entry.il_apys["inception"]
          ).toFixed(2),
        }
      );
    }
    if (poolInfo.pool_age_days >= 90) {
      if (entry.fee_apys["90d"] !== -1 && entry.il_apys["90d"] !== -1) {
        results.push({
          name: "90D Avg",
          "Fees APY": entry.fee_apys["90d"].toFixed(2),
          "IL APY": entry.il_apys["90d"].toFixed(2),
          "Net APY": (entry.fee_apys["90d"] - entry.il_apys["90d"]).toFixed(2),
        });
      }
    }
    if (poolInfo.pool_age_days >= 60) {
      if (entry.fee_apys["60d"] !== -1 && entry.il_apys["60d"] !== -1) {
        results.push({
          name: "60D Avg",
          "Fees APY": entry.fee_apys["60d"].toFixed(2),
          "IL APY": entry.il_apys["60d"].toFixed(2),
          "Net APY": (entry.fee_apys["60d"] - entry.il_apys["60d"]).toFixed(2),
        });
      }
    }
    if (poolInfo.pool_age_days >= 30) {
      results.push({
        name: "30D Avg",
        "Fees APY": entry.fee_apys["30d"].toFixed(2),
        "IL APY": entry.il_apys["30d"].toFixed(2),
        "Net APY": (entry.fee_apys["30d"] - entry.il_apys["30d"]).toFixed(2),
      });
    }
    if (poolInfo.pool_age_days >= 14) {
      results.push({
        name: "14D Avg",
        "Fees APY": entry.fee_apys["14d"].toFixed(2),
        "IL APY": entry.il_apys["14d"].toFixed(2),
        "Net APY": (entry.fee_apys["14d"] - entry.il_apys["14d"]).toFixed(2),
      });
    }
    if (poolInfo.pool_age_days >= 7) {
      results.push({
        name: "7D Avg",
        "Fees APY": entry.fee_apys["7d"].toFixed(2),
        "IL APY": entry.il_apys["7d"].toFixed(2),
        "Net APY": (entry.fee_apys["7d"] - entry.il_apys["7d"]).toFixed(2),
      });
    }

    results.push({
      name: "Yesterday",
      "Fees APY": entry.fee_apys["1d"].toFixed(2),
      "IL APY": entry.il_apys["1d"].toFixed(2),
      "Net APY": (entry.fee_apys["1d"] - entry.il_apys["1d"]).toFixed(2),
    });

    return results;
  }

  useEffect(() => {
    const convertData = () => {
      if (poolInfo) {
        console.log(poolInfo["il_apys"]);

        setBarChartData(convertToChartData(poolInfo));
      }
    };
    convertData();
  }, [poolInfo]);

  function renderPriceChange(origPrice, currPrice) {
    const gainPct = ((currPrice - origPrice) / origPrice) * 100;
    return <PctGainLossSpan val={gainPct}>%</PctGainLossSpan>;
  }

  const CustomizedAxisTick = (props) => {
    const { x, y, payload } = props;

    return (
      <g transform={`translate(${x},${y})`}>
        <text
          x={0}
          y={0}
          dy={16}
          textAnchor={isMobile ? "end" : "middle"}
          verticalAnchor="middle"
          fill="#666"
          transform={isMobile ? "rotate(-25)" : "rotate(0)"}
        >
          {payload.value}
        </text>
      </g>
    );
  };

  const CustomTooltip = ({ active, payload, label }) => {
    if (active) {
      return (
        <div className="chart-hover">
          <p style={{ color: "#0FB1D8" }}>{`Fees APY: ${displayPrettyNumber(
            payload[0]?.value
          )}%`}</p>
          <p style={{ color: "#042B4E" }}>{`IL APY: ${displayPrettyNumber(
            payload[1]?.value
          )}%`}</p>
          <p style={{ color: "#5652CC" }}>{`Net APY: ${payload[2]?.value}%`}</p>
        </div>
      );
    }
    return null;
  };

  function renderBarChart() {
    function getMinBarData() {
      const number = Math.min(...barChartData.map((entry) => entry["Net APY"]));
      const number2 = Math.min(
        ...barChartData.map((entry) => entry["Fees APY"])
      );
      const number3 = Math.min(...barChartData.map((entry) => entry["IL APY"]));
      return Math.min(number, number2, number3);
    }

    function getMaxBarData() {
      const number = Math.max(...barChartData.map((entry) => entry["Net APY"]));
      const number2 = Math.max(
        ...barChartData.map((entry) => entry["Fees APY"])
      );
      const number3 = Math.max(...barChartData.map((entry) => entry["IL APY"]));
      return Math.max(number, number2, number3);
    }
    return (
      <>
        {barChartData ? (
          <BarContainer style={{ width: "100%", height: 350 }} className="mb-5">
            <ResponsiveContainer>
              <BarChart
                height={350}
                data={barChartData}
                margin={{
                  top: 5,
                  right: 20,
                  left: 20,
                  bottom: 5,
                }}
              >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis
                  angle={-45}
                  dataKey="name"
                  interval={0}
                  dx={-20}
                  dy={10}
                  height={45}
                  tickLine={false}
                  tick={<CustomizedAxisTick />}
                />
                <YAxis
                  tickFormatter={(tick) => `${tick}%`}
                  padding={{ top: 20, bottom: 20 }}
                  domain={[
                    (dataMin) => getMinBarData(),
                    (dataMax) => getMaxBarData(),
                  ]}
                  allowDataOverflow={true}
                  label={{
                    angle: -90,
                    position: "insideLeft",
                  }}
                />
                <Tooltip content={<CustomTooltip />} />
                <Legend verticalAlign="bottom" />

                <Bar
                  dataKey="Fees APY"
                  barSize={15}
                  maxBarSize={250}
                  minPointSize={3}
                  fill="#0FB1D8"
                />
                <Bar
                  dataKey="IL APY"
                  barSize={15}
                  maxBarSize={250}
                  minPointSize={3}
                  fill="#042B4E"
                />
                <Bar
                  dataKey="Net APY"
                  barSize={15}
                  maxBarSize={250}
                  minPointSize={3}
                  fill="#5652CC"
                />
              </BarChart>
            </ResponsiveContainer>
          </BarContainer>
        ) : null}
      </>
    );
  }

  function getIlTable() {
    const now = dayjs();
    const d2 = now.subtract(poolInfo.pool_age_days, "day");
    const poolStartedAt = d2.format("YYYY-MM-DD");

    function renderRow(
      title,
      poolPrices,
      feeApy,
      ilsForPeriodElement,
      ilApy,
      period
    ) {
      const prices = poolPrices.map((key) => key[period]);
      return prices[0] === 0 || prices[1] === 0 ? null : (
        <tr>
          <td className="date-col">{title}</td>
          {feeApy === -1 && ilApy === -1 && ilsForPeriodElement === -1 ? (
            <td colSpan={6} className="centered">
              <Link to="/membership">[GO VISION2K]</Link>
            </td>
          ) : (
            <>
              {prices.map((tokenPrice, index) => (
                <td>
                  ${toDecimalPlaces(tokenPrice)} (
                  {renderPriceChange(tokenPrice, currentPrices[index])})
                </td>
              ))}
              <td>{feeApy.toFixed(2)}%</td>
              <td>-{ilsForPeriodElement.toFixed(2)}%</td>
              <td>-{ilApy.toFixed(2)}%</td>
              <td>{(feeApy - ilApy).toFixed(2)}%</td>
            </>
          )}
        </tr>
      );
    }

    return (
      <div>
        <CardContainer>
          <Card>
            <div className="card-block row">
              <Col sm="12" lg="12" xl="12">
                {renderBarChart()}
              </Col>
            </div>
          </Card>
        </CardContainer>
        <h6
          style={{
            fontFamily: "Nunito Sans, sans-serif",
            fontSize: 18,
            fontWeight: 900,
            color: "#042B4E",
          }}
        >
          1y Fees - Impermanent Losses / Reserves APY
        </h6>
        <p
          style={{
            fontFamily: "Nunito Sans, sans-serif",
            fontSize: 14,
            fontWeight: 900,
            color: "#042B4E",
            marginBottom: 12,
          }}
        >
          This is the APY (Annual Percentage Yield) you would get we included
          the Impermanent Losses the pool incurred.
        </p>
        <CardContainer>
          <Card style={{ borderRadius: 24 }}>
            <div className="tableresponsive" style={{ overflowX: "auto" }}>
              <Table>
                <thead>
                  <tr style={{ borderTopColor: "#9FBACF" }}>
                    <th scope="col" className="date-col"></th>
                    {poolInfo.prices.map((token) => (
                      <th scope="col">Price {token.symbol} (% Change)</th>
                    ))}
                    <th scope="col">Avg Fees Period APY</th>
                    <th scope="col">IL % (Period)</th>
                    <th scope="col">IL APY</th>
                    <th scope="col">Fees - IL APY</th>
                  </tr>
                </thead>
                <tbody>
                  {renderRow(
                    "Since you entered",
                    poolInfo.prices,
                    poolInfo.fee_apys["1d"],
                    poolInfo.ils_for_period["1d"],
                    poolInfo.il_apys["1d"],
                    "1d_usd"
                  )}
                  {renderRow(
                    "Yesterday",
                    poolInfo.prices,
                    poolInfo.fee_apys["1d"],
                    poolInfo.ils_for_period["1d"],
                    poolInfo.il_apys["1d"],
                    "1d_usd"
                  )}
                  {poolInfo.pool_age_days >= 7
                    ? renderRow(
                        "Last 7D average",
                        poolInfo.prices,
                        poolInfo.fee_apys["7d"],
                        poolInfo.ils_for_period["7d"],
                        poolInfo.il_apys["7d"],
                        "7d_usd"
                      )
                    : null}
                  {poolInfo.pool_age_days >= 14
                    ? renderRow(
                        "Last 14D average",
                        poolInfo.prices,
                        poolInfo.fee_apys["14d"],
                        poolInfo.ils_for_period["14d"],
                        poolInfo.il_apys["14d"],
                        "14d_usd"
                      )
                    : null}
                  {poolInfo.pool_age_days >= 30
                    ? renderRow(
                        "Last 30D average",
                        poolInfo.prices,
                        poolInfo.fee_apys["30d"],
                        poolInfo.ils_for_period["30d"],
                        poolInfo.il_apys["30d"],
                        "30d_usd"
                      )
                    : null}
                  {poolInfo.pool_age_days >= 30
                    ? renderRow(
                        `Since Inception`,
                        poolInfo.prices,
                        poolInfo.fee_apys["inception"],
                        poolInfo.ils_for_period["inception"],
                        poolInfo.il_apys["inception"],
                        "inception"
                      )
                    : null}
                </tbody>
              </Table>
            </div>
          </Card>
        </CardContainer>
      </div>
    );
  }

  if (poolInfo && currentPrices) {
    return (
      <Col xl="12" style={{ paddingLeft: 0, paddingRight: 0 }}>
        {getIlTable()}
      </Col>
    );
  }
  return null;
}
