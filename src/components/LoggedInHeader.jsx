import React, { useEffect, useState } from "react";
import "../styles.css";
import { Link, NavLink, useLocation } from "react-router-dom";
import lproyale from "../assets/images/lp-royale-medium.svg";
import styled from "styled-components";
import { Nav, NavItem, Button } from "reactstrap";
import { isMobile } from "react-device-detect";
import SideBar from "../components/Sidebar";

const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: relative;
  height: 104px;
  width: 100%;
  max-width: 1188px;
  margin-left: auto;
  margin-right: auto;
  background: white;
  padding-left: 24px;
  padding-right: 24px;
  background-color: #f3f8fc;
  @media only screen and (max-width: 600px) {
    padding-left: 1.5rem;
    padding-right: 1.5rem;
  }
`;

export const StyledButton = styled(Button)`
  background-color: ${(props) => props.inputColor} !important;
  border: none;
  font-family: "MachineGunk";
  font-size: 22px !important;
  text-shadow: 0px 1px 0px #000000;
  -webkit-text-stroke: 0.5px #042b4e;
`;

const Left = styled.div`
  width: fit-content;
  height: fit-content;
  .currently-active {
    border-bottom: 2px solid #0fb1d8;
    a {
      color: #0fb1d8;
      border-bottom: none !important;
    }
  }
  a {
    font-size: 18px;
    font-weight: 400;
    color: black;
    font-family: "MachineGunk";
    text-decoration: none !important;
  }
`;

const Right = styled.div`
  width: fit-content;
  height: fit-content;
  .currently-active {
    border-bottom: 2px solid #0fb1d8;
    a {
      color: #0fb1d8;
      border-bottom: none !important;
    }
  }
  a {
    font-size: 18px;
    font-weight: 400;
    color: black;
    font-family: "MachineGunk";
    text-decoration: none !important;
  }
`;

const Middle = styled.div`
  width: fit-content;
  height: fit-content;
  position: absolute;
  left: calc(50% - (98px));
  @media only screen and (max-width: 600px) {
    position: relative;
    left: 0px;
  }
`;

const LoggedInHeader = (props) => {
  const location = useLocation();
  const currentLocation = location.pathname.slice(1, location.pathname.length);
  const [activeTab, setActiveTab] = useState(currentLocation);
  return (
    <Container id="Container">
      {!isMobile && (
        <Left>
          <Nav>
            <NavItem
              style={{ height: "fit-content" }}
              className={`pt-3 pb-2 pr-3 pl-3 ${
                activeTab === "dashboard" ? "currently-active" : ""
              }`}
            >
              <NavLink
                onClick={() => setActiveTab("dashboard")}
                to="/dashboard"
              >
                Dashboard
              </NavLink>
            </NavItem>
            <NavItem
              style={{ height: "fit-content" }}
              className={`pt-3 pb-2 pr-3 pl-3 ${
                activeTab === "all-pools" ? "currently-active" : ""
              }`}
            >
              <NavLink
                onClick={() => setActiveTab("all-pools")}
                to="/all-pools"
              >
                All Pools
              </NavLink>
            </NavItem>
            <NavItem
              style={{ height: "fit-content" }}
              className={`pt-3 pb-2 pr-3 pl-3 ${
                activeTab === "leaderboard" ? "currently-active" : ""
              }`}
            >
              <NavLink
                onClick={() => setActiveTab("leaderboard")}
                to="/leaderboard"
              >
                Leaderboard
              </NavLink>
            </NavItem>
          </Nav>
        </Left>
      )}
      <Middle>
        <img src={lproyale} />
      </Middle>
      {isMobile && (
        <SideBar
          right
          pageWrapId={"Container"}
          outerContainerId={"Container"}
        />
      )}
      {!isMobile && (
        <Right>
          <Nav>
            <NavItem
              style={{ height: "fit-content" }}
              className={`pt-3 pb-2 pr-3 pl-3 ${
                activeTab === "tutorial" ? "currently-active" : ""
              }`}
            >
              <NavLink onClick={() => setActiveTab("tutorial")} to="/tutorial">
                Tutorial
              </NavLink>
            </NavItem>
            <NavItem
              style={{ height: "fit-content" }}
              className={`pt-3 pb-2 pr-3 pl-3 ${
                activeTab === "about" ? "currently-active" : ""
              }`}
            >
              <NavLink onClick={() => setActiveTab("faq")} to="/faq">
                FAQ
              </NavLink>
            </NavItem>
            <NavItem
              style={{ height: "fit-content" }}
              className={`pt-3 pb-2 pr-3 pl-3 ${
                activeTab === "logout" ? "currently-active" : ""
              }`}
            >
              <NavLink onClick={() => setActiveTab("logout")} to="/logout">
                Logout
              </NavLink>
            </NavItem>
          </Nav>
        </Right>
      )}
    </Container>
  );
};

export default LoggedInHeader;
