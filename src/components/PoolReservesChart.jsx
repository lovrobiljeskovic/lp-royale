import React, { useEffect, useState } from "react";
import {
  Alert,
  Breadcrumb,
  BreadcrumbItem,
  Card,
  CardHeader,
  Col,
  Container,
  Row,
  Table,
} from "reactstrap";
import {
  displayPrettyNumber,
  getPoolIcon,
  toDecimalPlaces,
} from "../helpers/util";
import {
  Area,
  ComposedChart,
  Bar,
  Line,
  Brush,
  BarChart,
  CartesianGrid,
  Legend,
  ResponsiveContainer,
  Tooltip,
  LabelList,
  XAxis,
  YAxis,
} from "recharts";
import { uniswapClient } from "../components/uniswap/client";
import { toNiceDateYear } from "../components/uniswap/utils/utils";
import moment from "moment";
import axios from "axios";
import { PAIR_LIQUIDITY_VOLUME } from "./uniswap/queries";
import styled from "styled-components";
import { isMobile } from "react-device-detect";

const BarContainer = styled.div`
  .recharts-cartesian-axis-tick {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 800;
    font-size: 12px;
    text {
      fill: #042b4e;
    }
  }
  .recharts-cartesian-axis-tick-value {
    fill: #042b4e;
  }
  .recharts-legend-wrapper {
    bottom: -30px !important;
  }
  .recharts-default-legend {
    text-align: center !important;
    @media only screen and (max-width: 600px) {
      text-align: end !important;
      margin-left: 0px !important;
      .recharts-legend-item {
        margin-right: 0px !important;
      }
    }
  }
`;

export default function (props) {
  const [barChartData, setBarChartData] = useState();
  const [lengthGraph, setLengthGraph] = useState(0);
  const [trackedUsdZero, setTrackedUsdZero] = useState(false);
  const poolAddrs = props.poolAddrs;
  const market = props.market;

  const fetchPairLiquidityVolume = async () => {
    let results = null;
    let result = null;
    try {
      if (market === "uniswap") {
        result = await uniswapClient.query({
          query: PAIR_LIQUIDITY_VOLUME(poolAddrs),
          fetchPolicy: "cache-first",
        });
        results = result?.data?.pairDayDatas;
      }
    } catch (e) {
      console.log(e);
    }

    return [results];
  };

  useEffect(() => {
    async function checkForLiquidityVolumeData() {
      if (!barChartData) {
        let [liqVolumeData, error, data] = await fetchPairLiquidityVolume();
        setBarChartData(convertToChartData(liqVolumeData));
      }
    }

    checkForLiquidityVolumeData();
  }, []);

  function convertToChartData(arr) {
    const results = [];
    setTrackedUsdZero(
      !arr.some((item) => item.dailyVolumeUSD > 0 || item.volumeUSD > 0)
    );
    arr.map((item) => {
      let volumeUSD = item.dailyVolumeUSD || item.volumeUSD || 0;
      results.push({
        time: parseInt((new Date(item.date).getTime() / 1000).toFixed(0)),
        date: toNiceDateYear(item.date),
        Reserves: parseFloat(item.reserveUSD).toFixed(2),
        Volume: parseFloat(volumeUSD).toFixed(2),
        "V/R Ratio": (volumeUSD / item.reserveUSD).toFixed(2),
      });
    });
    setLengthGraph(results.length);
    return results;
  }

  function renderChart() {
    function getMinBarData() {
      const number = Math.min(
        ...barChartData.map((entry) => entry["V/R Ratio"])
      );
      return number;
    }

    function getMaxBarData() {
      const number = Math.max(
        ...barChartData.map((entry) => entry["Reserves"])
      );
      const number2 = Math.max(...barChartData.map((entry) => entry["Volume"]));
      return Math.max(number, number2);
    }

    const formatXAxis = (tickItem) => {
      return new Date(tickItem * 1000 * 1000).toLocaleString("utc", {
        month: "short",
        day: "numeric",
      });
    };

    const formatYAxis = (tickItem) => {
      return displayPrettyNumber(tickItem, true);
    };

    const CustomizedAxisTick = (props) => {
      const { x, y, payload } = props;
      const transformedValue = new Date(
        payload.value * 1000 * 1000
      ).toLocaleString("utc", {
        month: "short",
        day: "numeric",
      });
      return (
        <g transform={`translate(${x},${y})`}>
          <text
            x={0}
            y={0}
            dy={16}
            textAnchor={isMobile ? "end" : "middle"}
            verticalAnchor="middle"
            fill="#666"
            transform={isMobile ? "rotate(-25)" : "rotate(0)"}
          >
            {transformedValue}
          </text>
        </g>
      );
    };

    const CustomTooltip = ({ active, payload, label }) => {
      if (active && payload) {
        console.log("payload", payload);
        return (
          <div className="chart-hover">
            <p style={{ color: "#5652CC" }}>{`Reserves: ${displayPrettyNumber(
              payload[1]?.value
            )}`}</p>
            <p style={{ color: "#042B4E" }}>{`Volume: ${displayPrettyNumber(
              payload[2]?.value
            )}`}</p>
            <p
              style={{ color: "#0FB1D8" }}
            >{`V/R Ratio: ${payload[0]?.value}`}</p>
          </div>
        );
      }
      return null;
    };

    const renderCustomizedLabel = (props) => {
      const { x, y, width, height, value } = props;

      return (
        <g>
          <text
            x={x + width / 2}
            y={y - 10}
            fill="#8f51dd"
            textAnchor="middle"
            dominantBaseline="middle"
          >
            {value}
          </text>
        </g>
      );
    };

    return (
      <>
        {barChartData ? (
          <BarContainer style={{ width: "100%", height: 350 }} className="mb-5">
            <ResponsiveContainer width="99%">
              <ComposedChart
                data={barChartData}
                margin={{
                  top: 5,
                  right: 20,
                  left: 20,
                  bottom: 5,
                }}
              >
                <CartesianGrid strokeDasharray="5 5" />

                <XAxis
                  dataKey="time"
                  domain={["auto", "auto"]}
                  name="time"
                  tick={<CustomizedAxisTick />}
                  type="number"
                  allowDataOverflow={false}
                />
                <YAxis
                  domain={[(dataMin) => 0, (dataMax) => getMaxBarData()]}
                  allowDataOverflow={true}
                  tickFormatter={formatYAxis}
                  yAxisId="left"
                />
                <YAxis
                  yAxisId="bar"
                  allowDataOverflow={true}
                  orientation="left"
                  hide={true}
                />

                <Tooltip content={<CustomTooltip />} />
                <Legend verticalAlign="bottom" />
                <Bar
                  yAxisId="bar"
                  dataKey="V/R Ratio"
                  barSize={20}
                  fill="#0FB1D8"
                  minPointSize={3}
                ></Bar>
                <Line
                  yAxisId="left"
                  type="monotone"
                  dataKey="Reserves"
                  stroke="#5652CC"
                  strokeWidth={2}
                />
                <Line
                  yAxisId="left"
                  type="monotone"
                  dataKey="Volume"
                  stroke="#042B4E"
                  strokeWidth={2}
                />
                <Brush
                  dataKey="date"
                  height={20}
                  stroke="#000000"
                  y={320}
                  startIndex={lengthGraph > 16 ? lengthGraph - 16 : 0}
                >
                  <BarChart>
                    <Bar dataKey="V/R Ratio" fill="#0FB1D8" stackId="a" />
                  </BarChart>
                </Brush>
              </ComposedChart>
            </ResponsiveContainer>
          </BarContainer>
        ) : null}
      </>
    );
  }

  return (
    <>
      <div className="m-l-15 m-r-15">
        {trackedUsdZero ? (
          <Row className="justify-content-center">
            <Col lg="8" className="mb-3">
              <Alert className="alert-dismissible mt-3 mb-2" color="info">
                Please note that some Volume data is missing for this pool.
              </Alert>
            </Col>
          </Row>
        ) : null}
        <div className="card-block row">
          <Col sm="12" lg="12" xl="12">
            {renderChart()}
          </Col>
        </div>
      </div>
    </>
  );
}
