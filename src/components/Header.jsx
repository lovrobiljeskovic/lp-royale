import React, { useEffect, useState } from "react";
import "../styles.css";
import { Link, NavLink, useLocation } from "react-router-dom";
import lproyale from "../assets/images/lp-royale-medium.svg";
import styled from "styled-components";
import { Nav, NavItem, Button } from "reactstrap";
import { isMobile } from "react-device-detect";
import SideBar from "../components/Sidebar";
import { StyledButton } from "../components/ButtonsAndInputs";

const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: relative;
  height: 104px;
  width: 100%;
  padding-left: 24px;
  padding-right: 24px;
  background-color: #f3f8fc;
  @media only screen and (max-width: 600px) {
    padding-left: 1.5rem;
    padding-right: 1.5rem;
  }
`;

const Left = styled.div`
  width: fit-content;
  height: fit-content;
  .currently-active {
    border-bottom: 2px solid #0fb1d8;
    a {
      color: #0fb1d8;
      border-bottom: none !important;
    }
  }
  a {
    font-size: 18px;
    font-weight: 400;
    color: black;
    font-family: "MachineGunk";
    text-decoration: none !important;
  }
`;

const Right = styled.div`
  width: fit-content;
  height: fit-content;
`;

const Middle = styled.div`
  width: fit-content;
  height: fit-content;
  position: absolute;
  left: calc(50% - (98px));
  @media only screen and (max-width: 600px) {
    position: relative;
    left: 0px;
  }
`;

const Header = (props) => {
  const [activeTab, setActiveTab] = useState("home");
  const location = useLocation();

  return (
    <div style={{ backgroundColor: "#F3F8FC" }}>
      <div
        id="Header"
        style={{
          maxWidth: 1188,
          marginLeft: "auto",
          marginRight: "auto",
        }}
      >
        {location.pathname === "/signup" ||
        location.pathname === "/login" ||
        location.pathname === "/onboarding" ? null : (
          <Container id="Container">
            {!isMobile && (
              <Left>
                <Nav>
                  <NavItem
                    style={{ height: "fit-content" }}
                    className={`pt-3 pb-2 pr-3 pl-3 ${
                      activeTab === "home" ? "currently-active" : ""
                    }`}
                  >
                    <NavLink onClick={() => setActiveTab("home")} to="/">
                      Home
                    </NavLink>
                  </NavItem>
                  <NavItem
                    style={{ height: "fit-content" }}
                    className={`pt-3 pb-2 pr-3 pl-3 ${
                      activeTab === "about" ? "currently-active" : ""
                    }`}
                  >
                    <NavLink onClick={() => setActiveTab("about")} to="/about">
                      About
                    </NavLink>
                  </NavItem>
                  <NavItem
                    style={{ height: "fit-content" }}
                    className={`pt-3 pb-2 pr-3 pl-3 ${
                      activeTab === "faq" ? "currently-active" : ""
                    }`}
                  >
                    <NavLink onClick={() => setActiveTab("faq")} to="/faq">
                      FAQ
                    </NavLink>
                  </NavItem>
                </Nav>
              </Left>
            )}
            <Middle>
              <img src={lproyale} />
            </Middle>
            {isMobile && (
              <SideBar
                right
                pageWrapId={"Container"}
                outerContainerId={"Container"}
              />
            )}
            {!isMobile && (
              <Right>
                {" "}
                <Link to="/login">
                  <StyledButton size="lg">Login</StyledButton>
                </Link>
              </Right>
            )}
          </Container>
        )}
      </div>
    </div>
  );
};

export default Header;
