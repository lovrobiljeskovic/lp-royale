import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { InputGroup, Input, Button } from "reactstrap";
import rightArrow from "../assets/images/button-right.svg";
import { useLocation } from "react-router-dom";

const Container = styled.footer`
  display: flex;
  justify-content: space-between;
  padding: 44px 152px 74px 152px;
  h2 {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 900;
    font-size: 16px;
    line-height: 22px;
    text-transform: uppercase;
    margin-bottom: 16px;
  }
  a {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 800;
    font-size: 14px;
    line-height: 19px;
    margin-bottom: 8px;
  }
  @media only screen and (max-width: 600px) {
    padding: 20px;
    flex-direction: column;
  }
  @media only screen and (min-width: 600px) and (max-width: 1180px) {
    padding: 30px;
  }
`;

const NewsletterContainer = styled.div`
  h2 {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 900;
    font-size: 16px;
    line-height: 22px;
    text-transform: none;
  }
  .input-group {
    display: flex;
  }
  input {
    background: linear-gradient(180deg, #d6dcec 0%, #ffffff 100%);
    border: 1px solid #042b4e;
    box-shadow: inset 0px 2px 0px 1px rgba(0, 0, 0, 0.25);
    border-radius: 8px !important;
    width: 312px !important;
    @media only screen and (max-width: 600px) {
      margin-bottom: 8px;
    }
  }
  button {
    margin-left: 8px;
    background: linear-gradient(358.97deg, #0fb1d8 0.89%, #74c9dc 99.11%);

    border: 1px solid #042b4e;
    box-sizing: border-box;
    box-shadow: 0px 2px 0px rgba(4, 43, 78, 0.2),
      inset 0px -4px 0px rgba(4, 43, 78, 0.4);
    border-radius: 8px;
    @media only screen and (max-width: 600px) {
      margin-left: 0px;
    }
  }
`;

const BackgroundContainer = styled.div`
  background: linear-gradient(358.97deg, #9fbacf 0.89%, #c7d8e9 99.11%);
  border: 1px solid #000000;
  box-sizing: border-box;
  box-shadow: 0px 4px 0px rgba(4, 43, 78, 0.14);
`;

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  @media only screen and (max-width: 600px) {
    margin-bottom: 20px;
  }
`;

const Column = styled.div`
  display: flex;
  flex-direction: column;
  margin-right: ${(props) => props.margin};
  @media only screen and (max-width: 600px) {
    margin-right: 0rem;
  }
  @media only screen and (min-width: 600px) and (max-width: 1180px) {
    margin-right: 3rem;
  }
`;

export const Footer = () => {
  const location = useLocation();

  return (
    <div>
      {location.pathname === "/signup" ||
      location.pathname === "/login" ||
      location.pathname === "/onboarding" ? null : (
        <BackgroundContainer>
          <div
            style={{ maxWidth: 1440, marginLeft: "auto", marginRight: "auto" }}
          >
            <Container>
              <Row>
                <Column margin={"12rem"}>
                  <h2>HEADING</h2>
                  <a>Link 1</a>
                  <a>Link 2</a>
                  <a>Link 3</a>
                  <a>Link 4</a>
                </Column>
                <div className="d-flex flex-column">
                  <h2>HEADING</h2>
                  <a>Link 1</a>
                  <a>Link 2</a>
                  <a>Link 3</a>
                  <a>Link 4</a>
                </div>
              </Row>
              <NewsletterContainer>
                <h2>Sign up for LP Royale newsletter</h2>
                <InputGroup>
                  <Input placeholder="Your email" />
                  <Button>
                    <img src={rightArrow} />
                  </Button>
                </InputGroup>
              </NewsletterContainer>
            </Container>
          </div>
        </BackgroundContainer>
      )}
    </div>
  );
};
