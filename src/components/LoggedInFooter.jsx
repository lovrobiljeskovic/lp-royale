import React, { useEffect, useState } from "react";
import styled from "styled-components";
import coin from "../assets/images/coin-icon.svg";

const Container = styled.div`
  height: 88px;
  display: flex;
  max-width: 1188px;
  padding-left: 24px;
  padding-right: 24px;
  margin-left: auto;
  margin-right: auto;
  h2 {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 900;
    font-size: 16px;
    line-height: 22px;
    text-transform: uppercase;
    margin-bottom: 16px;
  }
  a {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 800;
    font-size: 14px;
    line-height: 19px;
    margin-bottom: 8px;
  }
  @media only screen and (max-width: 600px) {
    padding: 20px;
    flex-direction: column;
    height: fit-content;
  }
`;

const BackgroundContainer = styled.footer`
  background: #5652cc;
  position: absolute;
  bottom: 0;
  width: 100%;
  @media only screen and (max-width: 600px) {
    position: relative;
  }
`;

const ImageContainer = styled.div`
  position: absolute;
  bottom: 40px;
  @media only screen and (max-width: 600px) {
    bottom: 224px;
    left: 130px;
  }
`;

const ContentContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-left: 100px;
  align-items: center;
  width: 100%;
  @media only screen and (max-width: 600px) {
    margin-left: 0px;
    flex-direction: column;
    > * {
      &:first-child {
        margin-top: 40px;
        margin-bottom: 20px;
      }
    }
  }
`;

const Column = styled.div`
  display: flex;
  flex-direction: column;
  h2 {
    font-family: "MachineGunk";
    font-style: normal;
    font-weight: normal;
    font-size: 24px;
    text-shadow: 0px 1px 0px #000000;
    -webkit-text-stroke: 0.5px black;
    color: white;
    margin-bottom: 0px;
  }
  p {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 800;
    font-size: 14px;
    margin-bottom: 0px;
    color: white;
  }
`;

const ArticleColumn = styled.div`
  display: flex;
  p {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 800;
    font-size: 14px;
    margin-bottom: 0px;
    color: white;
  }
  div {
    width: 80px;
    height: 56px;
    background: #ffffff;
    margin-right: 12px;
  }
  @media only screen and (max-width: 600px) {
    width: 100%;
  }
`;

export const LoggedInFooter = () => {
  return (
    <BackgroundContainer>
      <Container>
        <ImageContainer>
          <img src={coin} />
        </ImageContainer>
        <ContentContainer>
          <Column>
            <h2>Hint text goes here</h2>
            <p>Here are some resources to help you dive deeper:</p>
          </Column>
          <ArticleColumn>
            <div />
            <p>
              Name of the article or <br />
              resource that makes sense
              <br /> here
            </p>
          </ArticleColumn>
          <ArticleColumn>
            <div />
            <p>
              Name of the article or <br />
              resource that makes sense
              <br /> here
            </p>
          </ArticleColumn>
        </ContentContainer>
      </Container>
    </BackgroundContainer>
  );
};
