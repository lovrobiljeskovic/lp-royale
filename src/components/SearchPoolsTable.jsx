import React, { useState } from "react";
import {
  Form,
  FormGroup,
  Input,
  Label,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";
import { toast, ToastContainer } from "react-toastify";
import { useLocalStorage } from "../helpers/util";
import styled from "styled-components";
import {
  StyledButton,
  StyledInput,
  StyledPrependInput,
  StyledPrependAppendInput,
} from "../components/ButtonsAndInputs";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: 20px;
  .row {
    margin-left: 0px !important;
  }
  .input-group {
    border-radius: 8px;
    flex-wrap: nowrap;
  }
  .input-group-text {
    border-top-left-radius: 8px;
    border-bottom-left-radius: 8px;
    border-top-right-radius: 8px;
    border-bottom-right-radius: 8px;
    border: none !important;
  }
  .form-control {
    background: linear-gradient(180deg, #d6dcec 0%, #ffffff 100%);
    border: 1px solid #042b4e;
  }
  .input-group-prepend {
    span {
      background: #042b4e;
      color: white;
      font-weight: 800;
    }
  }
  .input-group-append {
    span {
      background: #042b4e;
      color: white;
      font-weight: 800;
    }
  }
  .label {
    font-size: 14px;
    font-weight: 900;
    text-transform: uppercase;
    color: #042b4e;
  }
  @media only screen and (max-width: 1180px) {
    align-items: center;
    /* > * {
      flex-direction: column;
      height: fit-content;
      width: 60%;
    } */
    .form-group {
      width: 100%;
      max-width: 100% !important;
    }
  }
  @media only screen and (max-width: 600px) {
    > * {
      &:first-child {
        flex-direction: column;
      }
    }
  }
`;

export const SearchPoolsTable = (props) => {
  function formatNumber(num) {
    const firstVal = num.replace(/,/gi, "");
    return firstVal.split(/(?=(?:\d{3})+$)/).join(",");
  }

  return (
    <Container>
      <Form
        onSubmit={(e) => {
          e.preventDefault();
          props.setShouldSearch(true);
        }}
        className="d-flex align-items-center justify-content-between"
      >
        <FormGroup style={{ maxWidth: "248px", marginRight: "1em", flex: 1 }}>
          <Label className="label">Age</Label>
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <InputGroupText>=</InputGroupText>
            </InputGroupAddon>
            <StyledPrependAppendInput
              onChange={(e) => props.setSearchAge(formatNumber(e.target.value))}
              value={props.searchAge}
              type="text"
              placeholder="7"
            />
            <InputGroupAddon addonType="append">
              <InputGroupText>days</InputGroupText>
            </InputGroupAddon>
          </InputGroup>
        </FormGroup>
        <FormGroup style={{ maxWidth: "248px", marginRight: "1em", flex: 1 }}>
          <Label className="label">APY</Label>
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <InputGroupText>=</InputGroupText>
            </InputGroupAddon>
            <StyledPrependInput
              type="text"
              value={props.searchVr}
              onChange={(e) => props.setSearchVr(formatNumber(e.target.value))}
              placeholder="0"
            />
          </InputGroup>
        </FormGroup>
        <FormGroup style={{ maxWidth: "248px", marginRight: "1em", flex: 1 }}>
          <Label className="label">Volume</Label>
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <InputGroupText>=</InputGroupText>
            </InputGroupAddon>
            <StyledPrependAppendInput
              value={props.searchVolume}
              onChange={(e) =>
                props.setSearchVolume(formatNumber(e.target.value))
              }
              type="text"
              placeholder="50,000"
            />
            <InputGroupAddon addonType="append">
              <InputGroupText> USD</InputGroupText>
            </InputGroupAddon>
          </InputGroup>
        </FormGroup>
        <FormGroup style={{ maxWidth: "248px", flex: 1 }}>
          <Label className="label">Reserve</Label>
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <InputGroupText>=</InputGroupText>
            </InputGroupAddon>
            <StyledPrependAppendInput
              value={props.searchReserve}
              onChange={(e) =>
                props.setSearchReserve(formatNumber(e.target.value))
              }
              type="text"
              placeholder="250,000"
            />
            <InputGroupAddon addonType="append">
              <InputGroupText> USD</InputGroupText>
            </InputGroupAddon>
          </InputGroup>
        </FormGroup>
      </Form>
      <ToastContainer />
      <StyledButton
        size="md"
        type="submit"
        style={{ width: "fit-content", alignSelf: "flex-end" }}
      >
        Filter
      </StyledButton>
    </Container>
  );
};
