import styled from "styled-components";

const handleFontSize = (size) => {
  switch (size) {
    case "xl":
      return "28px";
    case "lg":
      return "22px";
    case "md":
      return "18px";
    case "sm":
      return "16px";
    default:
      return "18px";
  }
};

const handlePadding = (size) => {
  switch (size) {
    case "xl":
      return "10px 12px 12px 12px";
    case "lg":
      return " 8px 12px 10px 12px";
    case "md":
      return "7px 12px 8px 12px";
    case "sm":
      return "3px 8px 6px 8px";
    case "icon":
      return "9px 12px 11px";
    default:
      return "3px 8px 6px 8px";
  }
};

export const StyledButton = styled.button`
  font-family: "MachineGunk";
  color: #fff;
  border: 1px solid #042b4e;
  -webkit-text-stroke: 1px #000000;
  text-shadow: 0 1px 0 #000000;
  box-shadow: rgb(0 0 0 / 20%) 0px 2px 0px, rgb(0 0 0 / 20%) 0px -4px 0px inset;
  border-radius: 8px;
  line-height: initial;
  cursor: pointer;
  box-sizing: border-box;
  background: ${(props) =>
    props.primary
      ? "linear-gradient(38deg,#5d45b3 0%,#6b59d7 46.99%,#713ae7 47%,#49a8ff 100%)"
      : "linear-gradient(0deg,rgb(15, 177, 216) 0%,rgb(116, 201, 220) 100%);"};
  &:hover {
    box-shadow: ${(props) =>
      props.primary
        ? "rgb(0 0 0 / 20%) 0px 4px 0px inset"
        : "rgb(0 0 0 / 20%) 0px 4px 0px inset"};
  }
  font-size: ${({ size }) => handleFontSize(size)};
  padding: ${({ size }) => handlePadding(size)};
`;


export const StyledInput = styled.input`
  background: linear-gradient(180deg, #d6dcec 0%, #ffffff 100%);
  border: 1px solid #042b4e;
  box-shadow: inset 0px 2px 0px 1px rgba(0, 0, 0, 0.25);
  border-radius: 8px;
  height: 42px;
  padding: 0 16px;
  color: #042b4e;
  font-weight: 800;
  font-size: 16px;
  width: 100%;
  input::placeholder {
    color: #9fbacf;
  }
`;

export const StyledPrependAppendInput = styled.input`
  background: linear-gradient(180deg, #d6dcec 0%, #ffffff 100%);
  border: 1px solid #042b4e;
  box-shadow: inset 0px 2px 0px 1px rgba(0, 0, 0, 0.25);
  height: 42px;
  padding: 0 16px;
  color: #042b4e;
  font-weight: 800;
  font-size: 16px;
  width: 100%;
  input::placeholder {
    color: #9fbacf;
  }
`;

export const StyledPrependInput = styled.input`
  background: linear-gradient(180deg, #d6dcec 0%, #ffffff 100%);
  border: 1px solid #042b4e;
  box-shadow: inset 0px 2px 0px 1px rgba(0, 0, 0, 0.25);
  height: 42px;
  padding: 0 16px;
  color: #042b4e;
  font-weight: 800;
  font-size: 16px;
  width: 100%;
  border-top-right-radius: 8px;
  border-bottom-right-radius: 8px;
  input::placeholder {
    color: #9fbacf;
  }
`;

export const StyledSelect = styled.select`
  background: linear-gradient(180deg, #d6dcec 0%, #ffffff 100%);
  border: 1px solid #042b4e;
  box-shadow: inset 0px 2px 0px 1px rgba(0, 0, 0, 0.25);
  border-radius: 8px;
  height: 42px;
  padding: 0 16px;
  color: #042b4e;
  font-weight: 800;
  font-size: 16px;
  width: 100%;
  input::placeholder {
    color: #9fbacf;
  }
`;

// .xl {
//   font-size: 28px;
//   padding: 10px 12px 12px 12px;
// }

// .lg {
//   font-size: 22px;
//   padding: 8px 12px 10px 12px;
// }

// .md {
//   font-size: 18px;
//   padding: 7px 12px 8px 12px;
// }

// .sm {
//   font-size: 18px;
//   padding: 3px 8px 6px 8px;
// }

// .primary:hover {
//   box-shadow: rgb(0 0 0 / 20%) 0px 4px 0px inset;
// }

// .secondary:hover {
//   background: linear-gradient(0deg, rgb(116, 201, 220) 0%, rgb(15, 177, 216) 100%);
//   box-shadow: rgb(0 0 0 / 20%) 0px 4px 0px inset;
// }
