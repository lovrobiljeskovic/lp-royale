import React from "react";
import styled from "styled-components";
import pattern from "../assets/images/pattern.svg";
import placeholder from "../assets/images/placeholder.svg";
import DoubleTokenLogo from "./DoubleTokenLogo";
import { isMobile } from "react-device-detect";
import { StyledButton } from "./ButtonsAndInputs";
import { Card, Table } from "reactstrap";
import PctGainLossSpan from "./PctGainLossSpan";

const BackgroundContainer = styled.div`
  .topHalf {
    background: url(${pattern}), #0fb1d8;
    height: 272px;
    width: 100%;
  }
  .bottomHalf {
    background-color: #f3f8fc;
    height: 1208px;
    width: 100%;
    position: absolute;
    z-index: -1;
    @media only screen and (max-width: 980px) {
      > * {
        &:first-child {
          margin-top: 450px !important;
        }
      }
    }
  }
  h1 {
    font-family: "MachineGunk";
    font-style: normal;
    font-weight: normal;
    font-size: 28px;
    line-height: 90%;
    text-align: center;
    color: #ffffff;
    text-shadow: 0px 2px 0px #042b4e;
  }
  p {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 900;
    font-size: 14px;
    line-height: 19px;
    text-align: center;
    color: #ffffff;
    text-shadow: 0px 1px 0px #042b4e;
  }
`;

const Container = styled.div`
  background: none !important;
  max-width: 1188px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin-left: auto;
  margin-right: auto;
  background: white;
  padding-left: 24px;
  padding-right: 24px;
  position: relative;
  @media only screen and (max-width: 600px) {
    padding-left: 1.5rem;
    padding-right: 1.5rem;
    margin-right: 0px;
    margin-left: 0px;
  }
  h1 {
    font-size: 24px;
    color: #042b4e;
    text-shadow: none;
    margin-bottom: 0px;
  }
`;

const HeaderContainer = styled.div`
  display: flex;
  padding-left: 24px;
  padding-right: 24px;
  padding-top: 24px;
  padding-bottom: 24px;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 132px;
  padding-left: 10px;
  padding-right: 10px;
  > *:not(:last-child) {
    margin-right: 24px;
  }
  background: linear-gradient(
    37.78deg,
    #5d45b3 -2.02%,
    #6b59d7 47.31%,
    #713ae7 47.32%,
    #49a8ff 103.26%
  );
  border: 1px solid #042b4e;
  box-sizing: border-box;
  box-shadow: 0px 2px 0px rgba(4, 43, 78, 0.2),
    inset 0px -4px 0px rgba(4, 43, 78, 0.4);
  border-radius: 24px;
  @media only screen and (max-width: 980px) {
    flex-direction: column;
    height: fit-content;
    padding-left: 24px;
    padding-right: 24px;
    padding-top: 24px;
    padding-bottom: 24px;
    width: 80%;
    > *:not(:last-child) {
      margin-bottom: 10px;
      margin-right: 0px;
    }
  }
  @media only screen and (max-width: 600px) {
    width: 100%;
    flex-direction: column;
    height: fit-content;
    padding-left: 24px;
    padding-right: 24px;
    padding-top: 24px;
    padding-bottom: 24px;
    > *:not(:last-child) {
      margin-bottom: 10px;
      margin-right: 0px;
    }
  }
`;

const HeaderContent = styled.div`
  background: linear-gradient(180deg, #d6dcec 0%, #ffffff 100%);
  border: 1px solid #042b4e;
  box-shadow: inset 0px 2px 0px 1px rgba(0, 0, 0, 0.25);
  border-radius: 12px;
  height: 84px;
  width: 254px;
  display: flex;
  justify-content: space-between;
  padding-left: 10px;
  padding-right: 10px;
  padding-top: 10px;
  padding-bottom: 10px;
  h2 {
    margin-bottom: 0px;
    font-family: "MachineGunk";
    color: #5652cc;
  }
  p {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 900;
    font-size: 12px;
    margin-bottom: 8px;
    text-transform: uppercase;
    color: #042b4e;
  }
  span {
    font-family: "MachineGunk";
    color: #9fbacf;
    font-size: 14px;
  }
  @media only screen and (max-width: 1180px) {
    width: 100%;
  }
`;

const CardContainer = styled.div`
  width: 70%;
  .card {
    margin-bottom: 24px;
    background: #ffffff;
    border: 1px solid #9fbacf;
    box-sizing: border-box;
    box-shadow: 0px 4px 0px rgba(4, 43, 78, 0.14);
    border-radius: 0px 0px 24px 24px;
  }
  .table {
    margin-bottom: 0px !important;
  }
  .table td {
    width: 33%;
    text-align: center;
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 900;
    font-size: 14px;
    line-height: 19px;
    color: #042b4e;
    padding-top: 20px !important;
    padding-bottom: 20px !important;
    padding-left: 20px !important;
    padding-right: 20px !important;
    vertical-align: middle;
    span {
      font-family: "Nunito Sans", sans-serif;
      font-style: normal;
      font-weight: bold;
      font-size: 14px;
      line-height: 16px;
      display: flex;
      align-items: center;
      color: #042b4e;
      margin-top: 8px;
      text-align: center;
      display: flex;
      justify-content: center;
    }
    h3 {
      font-family: "Nunito Sans", sans-serif;
      font-style: normal;
      font-weight: bold;
      font-size: 12px;
      line-height: 16px;
      display: flex;
      align-items: center;
      color: #042b4e;
      margin-top: 8px;
    }
    @media only screen and (max-width: 600px) {
      width: auto;
    }
  }
  .table td:first-child {
    text-align: start;
  }
  .table th {
    text-align: center;
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 900;
    font-size: 12px;
    line-height: 16px;
    text-align: center;
    color: #042b4e;
    text-transform: uppercase;
    padding-top: 20px !important;
    padding-bottom: 20px !important;
    padding-left: 20px !important;
    padding-right: 20px !important;
    vertical-align: middle;
  }
  @media only screen and (max-width: 600px) {
    width: 100%;
  }
`;

export const MoreInfo = () => {
  return (
    <BackgroundContainer>
      <div className="topHalf align-items-center">
        <div
          className="d-flex flex-column justify-content-center align-items-center"
          style={{ width: "100%", height: "86%" }}
        >
          <div style={{ marginRight: 20, marginBottom: 12 }}>
            <DoubleTokenLogo
              size={64}
              style={
                isMobile
                  ? { position: "absolute", right: "20px" }
                  : { right: "0px" }
              }
            />
          </div>
          <h1>Your position on ETH/LINK</h1>
          <p>You entered 14 days ago</p>
        </div>
        <Container style={{ top: -24 }}>
          <HeaderContainer>
            <HeaderContent>
              <div className="d-flex flex-column">
                <p>Initial capital provided</p>
                <div className="d-flex align-items-baseline">
                  <h2>?</h2>
                  <span>/3581</span>
                </div>
              </div>
              <div
                className="d-flex flex-column justify-content-start"
                style={{ marginTop: 2 }}
              >
                <img src={placeholder} />
              </div>
            </HeaderContent>
            <HeaderContent>
              <div className="d-flex flex-column">
                <p>Gains</p>
                <div className="d-flex align-items-baseline">
                  <h2>$0</h2>
                  <span>/100,000</span>
                </div>
              </div>
              <div
                className="d-flex flex-column justify-content-start"
                style={{ marginTop: 2 }}
              >
                <img src={placeholder} />
              </div>
            </HeaderContent>
            <HeaderContent>
              <div className="d-flex flex-column">
                <p>Total gas paid</p>
                <div className="d-flex align-items-baseline">
                  <h2>$0</h2>
                </div>
              </div>
              <div
                className="d-flex flex-column justify-content-start"
                style={{ marginTop: 2 }}
              >
                <img src={placeholder} />
              </div>
            </HeaderContent>
            <HeaderContent>
              <div className="d-flex flex-column">
                <p>Liquidity pool gains</p>
                <div className="d-flex align-items-baseline">
                  <h2>0%</h2>
                </div>
              </div>
              <div
                className="d-flex flex-column justify-content-start"
                style={{ marginTop: 2 }}
              >
                <img src={placeholder} />
              </div>
            </HeaderContent>
          </HeaderContainer>
        </Container>
      </div>
      <div className="bottomHalf">
        <Container style={{ marginTop: 200 }}>
          <div
            className="d-flex justify-content-between align-items-center"
            style={
              isMobile
                ? { width: "100%", marginBottom: 14 }
                : { width: "70%", marginBottom: 14 }
            }
          >
            <h1>Tokens Balance</h1>
            <StyledButton primary size="md">
              Manage Liquidity
            </StyledButton>
          </div>
          <CardContainer>
            <Card style={{ borderRadius: 24 }}>
              <div className="tableresponsive" style={{ overflowX: "auto" }}>
                <Table>
                  <thead>
                    <tr>
                      <th scope="col"></th>
                      <th scope="col">Link</th>
                      <th scope="col">WETH</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Tokens Entered</td>
                      <td> 400</td>
                      <td> 11.24</td>
                    </tr>
                    <tr>
                      <td>Tokens now</td>
                      <td> 400</td>
                      <td> 11.24</td>
                    </tr>
                    <tr>
                      <td>Asset changes</td>
                      <td>
                        <PctGainLossSpan>400</PctGainLossSpan>
                      </td>
                      <td>
                        <PctGainLossSpan>400</PctGainLossSpan>
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </div>
            </Card>
          </CardContainer>
          <CardContainer>
            <Card style={{ borderRadius: 24 }}>
              <div className="tableresponsive" style={{ overflowX: "auto" }}>
                <Table>
                  <thead>
                    <tr>
                      <th scope="col"></th>
                      <th scope="col">Link</th>
                      <th scope="col">WETH</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Average entry token price</td>
                      <td> 400</td>
                      <td> 11.24</td>
                    </tr>
                    <tr>
                      <td>Current token price</td>
                      <td> 400</td>
                      <td> 11.24</td>
                    </tr>
                    <tr>
                      <td>Price Changes</td>
                      <td>
                        <PctGainLossSpan>400</PctGainLossSpan>
                      </td>
                      <td>
                        <PctGainLossSpan>11.24</PctGainLossSpan>
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </div>
            </Card>
          </CardContainer>
          <CardContainer>
            <Card style={{ borderRadius: 24 }}>
              <div className="tableresponsive" style={{ overflowX: "auto" }}>
                <Table>
                  <thead>
                    <tr>
                      <th scope="col"></th>
                      <th scope="col">Link</th>
                      <th scope="col">WETH</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        Initial tokens at initial prices <br />
                        <h3>Total: $11,204.58</h3>
                      </td>
                      <td> 400</td>
                      <td> 11.24</td>
                    </tr>
                    <tr>
                      <td>
                        Initial tokens at current prices <br />
                        <h3>Total: $11,204.58</h3>
                      </td>
                      <td> 400</td>
                      <td> 11.24</td>
                    </tr>
                    <tr>
                      <td>
                        Current assets at current prices <br />
                        <h3>Total: $11,204.58</h3>
                      </td>
                      <td>
                        {" "}
                        <PctGainLossSpan>400</PctGainLossSpan>
                      </td>
                      <td>
                        {" "}
                        <PctGainLossSpan>400</PctGainLossSpan>
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </div>
            </Card>
          </CardContainer>
        </Container>
      </div>
    </BackgroundContainer>
  );
};
