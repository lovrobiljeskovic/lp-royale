import React from "react";
import { Form, FormGroup, Input, Label, InputGroup } from "reactstrap";
import { toast, ToastContainer } from "react-toastify";
import styled from "styled-components";
import { StyledButton, StyledInput } from "../components/ButtonsAndInputs";

const Container = styled.div`
  display: flex;
  padding: 20px;
  justify-content: space-between;
  .row {
    margin-left: 0px !important;
    margin-right: 0px !important;
  }
  .input-group {
    border-radius: 8px;
  }
  .input-group-text {
    border: 1px solid #042b4e;
  }
  .form-control {
    background: linear-gradient(180deg, #d6dcec 0%, #ffffff 100%);
    border: 1px solid #042b4e;
  }
  .form-group {
    margin-bottom: 0px !important;
  }
  @media only screen and (max-width: 600px) {
    > * {
      &:first-child {
        flex-direction: column;
      }
    }
  }
`;

export const SearchTokenName = (props) => {
  return (
    <Container>
      <Form
        onSubmit={(e) => {
          e.preventDefault();
          props.setShouldSearch(true);
        }}
      >
        <FormGroup row>
          <InputGroup>
            <StyledInput
              onChange={(e) => props.setTokenName(e.target.value)}
              value={props.searchAge}
              type="text"
              placeholder="Token name"
            />
          </InputGroup>
        </FormGroup>
      </Form>
      <ToastContainer />
      <StyledButton size="md" type="submit">
        Filter
      </StyledButton>
    </Container>
  );
};
