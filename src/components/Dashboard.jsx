import React, { useEffect, useState } from "react";
import styled from "styled-components";
import placeholder from "../assets/images/placeholder.svg";
import { YourPoolsTable } from "./YourPoolsTable";
import { StyledButton } from "../components/ButtonsAndInputs";

const Container = styled.div`
  background: none !important;
  max-width: 1188px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin-left: auto;
  margin-right: auto;
  background: white;
  padding-left: 24px;
  padding-right: 24px;
  padding-top: 60px;
  position: relative;
  @media only screen and (max-width: 600px) {
    padding-left: 1.5rem;
    padding-right: 1.5rem;
    margin-right: 0px;
    margin-left: 0px;
  }
`;

const H1 = styled.h1`
  font-weight: 400;
  font-size: 32px !important;
  text-align: start;
  margin-bottom: 16px !important;
  font-family: "MachineGunk";
  color: #042b4e;
  @media only screen and (max-width: 600px) {
    font-size: 2.5rem !important;
    text-align: center;
  }
`;

const H2 = styled.h2`
  font-family: "MachineGunk";
  font-style: normal;
  font-weight: normal;
  font-size: 24px;
  color: #042b4e;
  margin-bottom: 0px !important;
  margin-right: 14px;
`;

const CardContainer = styled.div`
  display: flex;
  padding-left: 24px;
  padding-right: 24px;
  padding-top: 24px;
  padding-bottom: 24px;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 132px;
  padding-left: 10px;
  padding-right: 10px;
  > *:not(:last-child) {
    margin-right: 24px;
  }
  background: linear-gradient(
    37.78deg,
    #5d45b3 -2.02%,
    #6b59d7 47.31%,
    #713ae7 47.32%,
    #49a8ff 103.26%
  );
  border: 1px solid #042b4e;
  box-sizing: border-box;
  box-shadow: 0px 2px 0px rgba(4, 43, 78, 0.2),
    inset 0px -4px 0px rgba(4, 43, 78, 0.4);
  border-radius: 24px;
  @media only screen and (max-width: 1180px) {
    flex-direction: column;
    height: fit-content;
    padding-left: 24px;
    padding-right: 24px;
    padding-top: 24px;
    padding-bottom: 24px;
    width: 80%;
    > *:not(:last-child) {
      margin-bottom: 10px;
      margin-right: 0px;
    }
  }
  @media only screen and (max-width: 600px) {
    width: 100%;
    flex-direction: column;
    height: fit-content;
    padding-left: 24px;
    padding-right: 24px;
    padding-top: 24px;
    padding-bottom: 24px;
    > *:not(:last-child) {
      margin-bottom: 10px;
      margin-right: 0px;
    }
  }
`;

const Card = styled.div`
  background: linear-gradient(180deg, #d6dcec 0%, #ffffff 100%);
  border: 1px solid #042b4e;
  box-shadow: inset 0px 2px 0px 1px rgba(0, 0, 0, 0.25);
  border-radius: 12px;
  height: 84px;
  width: 254px;
  display: flex;
  justify-content: space-between;
  padding-left: 10px;
  padding-right: 10px;
  padding-top: 10px;
  padding-bottom: 10px;
  h2 {
    margin-bottom: 0px;
    font-family: "MachineGunk";
    color: #5652cc;
  }
  p {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 900;
    font-size: 12px;
    margin-bottom: 8px;
    text-transform: uppercase;
    color: #042b4e;
  }
  span {
    font-family: "MachineGunk";
    color: #9fbacf;
    font-size: 14px;
  }
  @media only screen and (max-width: 1180px) {
    width: 100%;
  }
`;

const NoPoolsContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin-top: 180px;
  p {
    font-style: normal;
    font-weight: 900;
    font-size: 18px;
    text-align: center;
    font-family: "Nunito Sans", sans-serif;
    color: #042b4e;
  }
  @media only screen and (max-width: 600px) {
    margin-top: 60px;
    margin-bottom: 60px;
  }
`;

export const Dashboard = () => {
  const data = [
    {
      poolName: "LINK/ETH",
      tokensEntered: "400 → 460",
      tokensEnteredName: "LINK",
      tokensExited: "11.24 → 10.04",
      tokensExitedName: "WETH",
      liquidityPoolGains: "$11,434.63",
    },
    {
      poolName: "KEK/ETH",
      tokensEntered: "400 → 460",
      tokensEnteredName: "KEK",
      tokensExited: "11.24 → 10.04",
      tokensExitedName: "WETH",
      liquidityPoolGains: "$11,434.63",
    },
  ];

  const [activePools, setActivePools] = useState(null);
  const [showMore, setShowMore] = useState(false);
  useEffect(() => {
    if (!activePools) {
      setActivePools(data);
    }
  }, [activePools]);
  return (
    <div style={{ backgroundColor: "#F3F8FC" }}>
      <Container>
        <H1>Your current pool positions</H1>
        <CardContainer>
          <Card>
            <div className="d-flex flex-column">
              <p>Current Leaderboard Rank</p>
              <div className="d-flex align-items-baseline">
                <h2>?</h2>
                <span>/3581</span>
              </div>
            </div>
            <div
              className="d-flex flex-column justify-content-start"
              style={{ marginTop: 2 }}
            >
              <img src={placeholder} />
            </div>
          </Card>
          <Card>
            <div className="d-flex flex-column">
              <p>Total Market Value</p>
              <div className="d-flex align-items-baseline">
                <h2>$0</h2>
                <span>/100,000</span>
              </div>
            </div>
            <div
              className="d-flex flex-column justify-content-start"
              style={{ marginTop: 2 }}
            >
              <img src={placeholder} />
            </div>
          </Card>
          <Card>
            <div className="d-flex flex-column">
              <p>Gas Fees + exit transactions</p>
              <div className="d-flex align-items-baseline">
                <h2>$0</h2>
              </div>
            </div>
            <div
              className="d-flex flex-column justify-content-start"
              style={{ marginTop: 2 }}
            >
              <img src={placeholder} />
            </div>
          </Card>
          <Card>
            <div className="d-flex flex-column">
              <p>Liquidity pool gains</p>
              <div className="d-flex align-items-baseline">
                <h2>0%</h2>
              </div>
            </div>
            <div
              className="d-flex flex-column justify-content-start"
              style={{ marginTop: 2 }}
            >
              <img src={placeholder} />
            </div>
          </Card>
        </CardContainer>
        {activePools ? (
          <div
            className="d-flex flex-column"
            style={{ width: "100%", marginTop: 80, marginBottom: 216 }}
          >
            <div
              className="d-flex align-items-center"
              style={{ marginBottom: 18 }}
            >
              <H2>Your Active Pools</H2>
              <StyledButton size="md">Find more pools</StyledButton>
            </div>
            <YourPoolsTable
              showMore={showMore}
              setShowMore={setShowMore}
              tableData={activePools}
            />
          </div>
        ) : (
          <NoPoolsContainer>
            <p>
              You haven’t entered any pool yet. <br /> Go spend that $100K!
            </p>
            <StyledButton primary size="xl">
              Find pools to invest in
            </StyledButton>
          </NoPoolsContainer>
        )}
      </Container>
    </div>
  );
};
