import React, { useState } from "react";
import { ArrowDownRight, ArrowUpRight } from "react-feather";
import DataTable, { createTheme } from "react-data-table-component";
import { displayPrettyNumber, getLinkForPool } from "../helpers/util";
import DoubleTokenLogo from "./DoubleTokenLogo";
import { Col } from "reactstrap";
import styled from "styled-components";
import { isMobile } from "react-device-detect";
import { Modal } from "reactstrap";
import "../Modal.css";
import {
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";
import ClipLoader from "react-spinners/ClipLoader";
import { StyledButton, StyledPrependInput } from "./ButtonsAndInputs";

createTheme("custom", {
  background: {
    default: "$primary-color",
  },
});

const customStyles = {
  rows: {
    style: {
      height: "80px", // override the row height
      textAlign: "center",
      borderTop: "1px solid #9FBACF",
    },
  },
  headCells: {
    style: {
      padding: "2px",
      minHeight: "90px",
      textAlign: "center",
      verticalAlign: "middle",
      fontFamily: "Nunito Sans, sans-serif",
      fontWeight: 900,
      textTransform: "uppercase",
      justifyContent: isMobile && "center",
      flexDirection: isMobile && "column",
    },
    activeSortStyle: {
      color: "$primary-color",
      "&:focus": {
        outline: "none",
      },
      "&:hover:not(:focus)": {
        color: "$primary-color",
      },
    },
    inactiveSortStyle: {
      "&:focus": {
        outline: "none",
        color: "$primary-color",
      },
      "&:hover": {
        color: "$primary-color",
      },
    },
  },
  table: {
    style: {
      borderCollapse: "collapse",
      borderLeft: "none",
      borderRight: "none",
      boxShadow: "0px 1px 0px #9FBACF",
      borderRadius: "23px 23px 0px 0px",
      borderTop: "1px solid #9FBACF",
      borderLeft: "1px solid #9FBACF",
      borderRight: "1px solid #9FBACF",
      borderBottom: "1px solid #9FBACF",
    },
  },
  cells: {
    style: {
      padding: ".38rem 0.3rem",
      verticalAlign: "middle",
      fontFamily: "Nunito Sans, sans-serif",
      fontWeight: 900,
    },
  },
  pagination: {
    pageButtonsStyle: {
      fill: "darkgray",
    },
  },
};

const ContentContainer = styled.div`
  display: flex;
`;

const Left = styled.div`
  margin-top: 12px;
  margin-bottom: 12px;
  margin-left: 12px;
  margin-right: 14px;
  border-radius: 24px;
  background-color: #f3f8fc;
  width: 45%;
  padding-top: 24px;
  padding-left: 16px;
  padding-right: 16px;
  padding-bottom: 12px;
  display: flex;
  flex-direction: column;
  p {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 900;
    font-size: 12px;
    color: #042b4e;
    text-transform: uppercase;
  }
  h1 {
    font-family: "MachineGunk";
    font-style: normal;
    font-weight: normal;
    font-size: 28px;
    color: #042b4e;
    text-align: center;
  }
  .input-group-prepend {
    span {
      background: #042b4e;
      color: white;
    }
  }
  .input-group-text {
    border-top-left-radius: 8px;
    border-bottom-left-radius: 8px;
    border-top-right-radius: 8px;
    border-bottom-right-radius: 8px;
    border: none !important;
  }
  .form-control {
    background: linear-gradient(180deg, #d6dcec 0%, #ffffff 100%);
    border: 1px solid #042b4e;
  }
  .form-group {
    width: 100%;
    margin-bottom: 8px;
  }
  .row {
    margin-left: 0px;
    margin-right: 0px;
  }
  @media only screen and (max-width: 600px) {
    width: 50%;
  }
`;
const Right = styled.div`
  margin-top: 12px;
  margin-bottom: 12px;
  margin-right: 12px;
  border-radius: 24px;
  background-color: #f3f8fc;
  display: flex;
  padding-top: 24px;
  padding-left: 16px;
  padding-right: 16px;
  padding-bottom: 12px;
  width: 55%;
  .no-amount {
    width: 100%;
    text-align: center;
    align-self: center;
  }
  span {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 800;
    font-size: 14px;
    color: #042b4e;
  }

  @media only screen and (max-width: 600px) {
    width: 50%;
  }
`;

const LogoContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  font-family: "Nunito Sans", sans-serif;
  font-style: normal;
  font-weight: 900;
  color: #042b4e;
  margin-top: 42px;
  margin-bottom: 62px;
  p {
    margin-bottom: 0px;
    margin-top: 8px;
    font-size: 22px;
  }
  > * {
    &:first-child {
      left: -20px;
    }
  }
`;

const TransactionDetails = styled.div`
  h2 {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 900;
    font-size: 12px;
    color: #9fbacf;
  }
`;

export const DisplayTable = (props) => {
  const [modal, setModal] = useState(false);
  const [amount, setAmount] = useState(null);
  const toggle = (pair) => {
    if (props.selectedPair) {
      props.setSelectedPair(null);
    } else {
      props.setSelectedPair(pair);
    }
    setModal(!modal);
  };
  const renderLastApySpan = (apyOverall, apyForPeriod) => {
    const isGain =
      apyOverall < apyForPeriod ? (
        <ArrowUpRight
          style={{ color: "#23B676" }}
          className="warning-icon"
          size={16}
        />
      ) : (
        <ArrowDownRight
          style={{ color: "#FF495C" }}
          className="warning-icon"
          size={16}
        />
      );
    return (
      <>
        <span>{apyForPeriod.toFixed(2)}%</span>
        {isGain}
      </>
    );
  };

  const columns = [
    {
      name: "Pool Name",
      selector: "name",
      sortable: true,
      minWidth: "11%",
      center: true,
      cell: (row) => (
        <b>
          <Col sm="12" md={{ size: 6, offset: 2 }} className="p-2">
            <DoubleTokenLogo size={24} a0={row.idToken0} a1={row.idToken1} />
          </Col>
          <a
            href={`${getLinkForPool(row.market, row.address)}`}
            target="_blank"
          >
            {row.name.length > 15
              ? `${row.name.substring(0, 15)}...`
              : row.name}
          </a>
        </b>
      ),
    },
    {
      name: "Volume 24H (USD)",
      selector: "volume_24h_usd",
      sortable: true,
      minWidth: "1%",
      center: true,
      cell: (row) => (
        <b>
          {row.volume_24h_usd < 1000
            ? "< $1K"
            : `$${displayPrettyNumber(row.volume_24h_usd)}`}
        </b>
      ),
    },
    {
      name: "Pool Reserve (USD)",
      selector: "pool_reserve_usd",
      sortable: true,
      minWidth: "11%",
      center: true,
      cell: (row) => (
        <b>
          {row.pool_reserve_usd < 1000
            ? "< $1K"
            : `$${displayPrettyNumber(row.pool_reserve_usd)}`}
        </b>
      ),
    },
    {
      name: "Impermanent Loss 24H",
      selector: "imp_loss_24h",
      sortable: true,
      minWidth: isMobile ? "16%" : "11%",
      center: true,
      cell: (row) => <b style={{ color: "#FF495C" }}>${row.imp_loss_24h} </b>,
    },
    {
      name: "Fee Collected 24H",
      selector: "fee_collected_usd",
      sortable: true,
      minWidth: "11%",
      center: true,
      cell: (row) => (
        <b style={{ color: "#23B676" }}>${row.fee_collected_usd}</b>
      ),
    },
    {
      name: "1y Fees / Reserves Since Inception",
      selector: "inception_average_fees_apy",
      sortable: true,
      minWidth: "11%",
      center: true,
      cell: (row) => <b>{row.inception_average_fees_apy}%</b>,
    },
    {
      name: "Last 7/14/30 Day Avg",
      selector: "average_apys_7d",
      sortable: true,
      minWidth: isMobile ? "16%" : "11%",
      center: true,
      cell: (row) => (
        <b>
          {row.age >= 7
            ? renderLastApySpan(
                row.inception_average_fees_apy,
                row.average_apys_7d
              )
            : "-"}
          <br />
          {row.age >= 14
            ? renderLastApySpan(
                row.inception_average_fees_apy,
                row.average_apys_14d
              )
            : "-"}
          <br />
          {row.age >= 30
            ? renderLastApySpan(
                row.inception_average_fees_apy,
                row.average_apys_30d
              )
            : "-"}
        </b>
      ),
    },
    {
      name: "",
      sortable: false,
      minWidth: isMobile ? "14%" : "22%",
      center: true,
      cell: (row) => (
        <div className={isMobile ? "d-flex flex-column" : "d-flex"}>
          <StyledButton primary size="sm" style={{ marginRight: 12 }}>
            Pool Stats
          </StyledButton>
          <StyledButton primary size="sm" onClick={() => toggle(row)}>
            Add liquidity
          </StyledButton>
        </div>
      ),
    },
  ];

  return (
    <div
      className="responsive"
      id="top-pools-table"
      style={{ width: "100%", marginBottom: 140, overflowX: "auto" }}
    >
      <DataTable
        columns={columns}
        data={props.tableData}
        customStyles={customStyles}
        theme="custom"
        pagination
        responsive={false}
        noHeader
        noDataComponent={
          <div style={{ padding: 20, border: "none" }}>
            <ClipLoader
              loading={props.loading}
              color="rgb(47, 128, 237)"
              size={150}
            />
          </div>
        }
      />
      <Modal
        isOpen={modal}
        toggle={() => toggle()}
        modalClassName="custom-modal"
      >
        <ContentContainer>
          <Left>
            <h1>Add liquidity to:</h1>
            {props.selectedPair && (
              <LogoContainer>
                <DoubleTokenLogo
                  size={64}
                  a0={props.selectedPair.idToken0}
                  a1={props.selectedPair.idToken1}
                />
                <p>{props.selectedPair.name}</p>
              </LogoContainer>
            )}
            <p>Available funds: $100,000</p>
            <Form
              onSubmit={(e) => {
                e.preventDefault();
                console.log("todo");
              }}
              className="d-flex align-items-center justify-content-around"
            >
              <FormGroup row>
                <InputGroup style={{ flexWrap: "nowrap" }}>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>$</InputGroupText>
                  </InputGroupAddon>
                  <StyledPrependInput
                    onChange={(e) => setAmount(e.target.value)}
                    value={amount}
                    type="text"
                    placeholder="Enter amount ..."
                  />
                </InputGroup>
              </FormGroup>
            </Form>
            <StyledButton size="lg">Confirm</StyledButton>
          </Left>
          <Right>
            {!amount ? (
              <div className="no-amount">
                <span>
                  Enter an amount to view <br /> the transaction costs details
                </span>
              </div>
            ) : (
              <TransactionDetails>
                <h2>TRANSACTION DETAILS</h2>
                
              </TransactionDetails>
            )}
          </Right>
        </ContentContainer>
      </Modal>
    </div>
  );
};
