import React, { useEffect, useState } from "react";
import axios from "axios";
import { processChartData } from "../helpers/util";
import { DisplayTable } from "./DisplayTable";
import { toast } from "react-toastify";
import { UNISWAP_INFO_API_URL } from "../constants";
import moment from "moment";
import styled from "styled-components";
import {
  Card,
  CardBody,
  Col,
  Nav,
  NavItem,
  NavLink,
  Row,
  TabContent,
  TabPane,
} from "reactstrap";
import { SearchPoolsTable } from "./SearchPoolsTable";
import { SearchTokenName } from "./SearchTokenName";

const Container = styled.div`
  background: none !important;
  max-width: 1188px;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  flex-direction: column;
  margin-left: auto;
  margin-right: auto;
  background: white;
  padding-left: 24px;
  padding-right: 24px;
  padding-top: 60px;
  position: relative;
  @media only screen and (max-width: 600px) {
    padding-left: 1.5rem;
    padding-right: 1.5rem;
    margin-right: 0px;
    margin-left: 0px;
  }
  .nav {
    .nav-tabs {
      border-bottom: none;
    }
    margin-left: 24px;
    .nav-link {
      border-radius: 16px 16px 0px 0px;
      font-family: "MachineGunk";
      color: black;
    }
    .active {
      background: #c7d8e9;
      color: white;
      -webkit-text-stroke: 0.5px #042b4e;
      text-shadow: 0px 1px 0px #000000;
    }
  }
`;

const H1 = styled.h1`
  font-weight: 400;
  font-size: 32px !important;
  text-align: start;
  margin-bottom: 16px !important;
  font-family: "MachineGunk";
  text-shadow: 0px 2px 0px #042b4e;
  color: #042b4e;
  @media only screen and (max-width: 600px) {
    font-size: 2.5rem !important;
  }
`;

const FilterContainer = styled.div`
  background: linear-gradient(358.97deg, #9fbacf 0.89%, #c7d8e9 99.11%);
  border: 1px solid #9fbacf;
  box-sizing: border-box;
  box-shadow: 0px 4px 0px rgba(4, 43, 78, 0.14);
  border-radius: 24px;
`;

const NavContainer = styled.div`
  display: flex;
  .nav-item {
    cursor: pointer;
  }
  @media only screen and (max-width: 600px) {
    width: 100%;
    margin-bottom: 26px;
    .nav {
      width: 100%;
      flex-direction: column;
      margin-left: 0px;
    }
  }
`;

export const AllPools = () => {
  const [tab, setTab] = useState("1");
  const [tableData, setTableData] = useState([]);
  const [searchVolume, setSearchVolume] = useState(50000);
  const [searchReserve, setSearchReserve] = useState(250000);
  const [shouldSearch, setShouldSearch] = useState(true);
  const [searchAge, setSearchAge] = useState(7);
  const [searchVr, setSearchVr] = useState(0);
  const [exchangesParam, setExchangesParam] = useState([
    "uniswap",
    "sushiswap",
    "balancer",
    "oneinch",
  ]);
  const [lastUpdated, setLastUpdated] = useState();
  const [gotResults, setGotResults] = useState(true);
  const [tokenName, setTokenName] = useState(null);
  let [loading, setLoading] = useState(true);
  const [selectedPair, setSelectedPair] = useState(null);
  const setResponseOrError = (response) => {
    if (response.data.results) {
      setTableData(processChartData(response.data.results));
      setGotResults(response.data.results.length > 0);
    }
    if (response.data.error) {
      setGotResults(false);
      toast.error(response.data.error, {
        position: toast.POSITION.TOP_CENTER,
        autoClose: 6000,
      });
      setLoading(!loading);
    }
  };

  const performCriteriaSearch = async () => {
    setTableData([]);
    setShouldSearch(false);
    setGotResults(true);
    const response = await axios.get(
      `${UNISWAP_INFO_API_URL}/api/v1/pool_search/advanced_search?avg_period_daily_volume_usd=${searchVolume}&avg_period_reserve_usd=${searchReserve}&min_pool_age_days=${searchAge}&vr=${searchVr}&exchanges=${exchangesParam}`
    );
    const d = new Date(parseInt(response.data.last_updated, 10) * 1000);
    setLastUpdated(moment(d).fromNow());
    setResponseOrError(response);
  };
  useEffect(() => {
    const fetchData = async () => {
      if (shouldSearch) {
        await performCriteriaSearch();
      }
    };

    fetchData();
  }, [shouldSearch]);

  const Cuck = () => {
    return <div></div>;
  };

  return (
    <div style={{ backgroundColor: "#F3F8FC" }}>
      <Container>
        <H1>All Pools</H1>
        <NavContainer>
          <Nav className="nav-primary" tabs>
            <NavItem>
              <NavLink
                className={tab === "1" ? "active" : ""}
                onClick={() => {
                  setTab("1");
                }}
              >
                <i className="icofont icofont-ui-home" />
                Filter by criteria
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={tab === "2" ? "active" : ""}
                onClick={() => {
                  setTab("2");
                }}
              >
                <i className="icofont icofont-listing-box" />
                Filter by token name
              </NavLink>
            </NavItem>
          </Nav>
        </NavContainer>
        <TabContent
          activeTab={tab}
          style={{ width: "100%", marginBottom: "60px" }}
        >
          <TabPane className="fade show" tabId="1">
            <FilterContainer>
              <SearchPoolsTable
                setShouldSearch={setShouldSearch}
                searchAge={searchAge}
                setSearchAge={setSearchAge}
                searchVolume={searchVolume}
                setSearchVolume={setSearchVolume}
                searchReserve={searchReserve}
                setSearchReserve={setSearchReserve}
                searchVr={searchVr}
                setSearchVr={setSearchVr}
                exchangesParam={exchangesParam}
                setExchangesParam={setExchangesParam}
              />
            </FilterContainer>
          </TabPane>
          <TabPane tabId="2">
            <FilterContainer>
              <SearchTokenName
                tokenName={tokenName}
                setTokenName={setTokenName}
              />
            </FilterContainer>
          </TabPane>
        </TabContent>
        <DisplayTable
          loading={loading}
          tableData={tableData}
          selectedPair={selectedPair}
          setSelectedPair={setSelectedPair}
        />
      </Container>
    </div>
  );
};
