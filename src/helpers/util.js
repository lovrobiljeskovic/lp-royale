import React, { useState } from "react";
import { uniswapClient } from "../components/uniswap/client";
import { ETH_PRICE } from "../components/uniswap/queries";

function decimalAdjust(type, value, exp) {
  // If the exp is undefined or zero...
  if (typeof exp === "undefined" || +exp === 0) {
    return Math[type](value);
  }
  value = +value;
  exp = +exp;
  // If the value is not a number or the exp is not an integer...
  if (isNaN(value) || !(typeof exp === "number" && exp % 1 === 0)) {
    return NaN;
  }
  // Shift
  value = value.toString().split("e");
  value = Math[type](+(value[0] + "e" + (value[1] ? +value[1] - exp : -exp)));
  // Shift back
  value = value.toString().split("e");
  return +(value[0] + "e" + (value[1] ? +value[1] + exp : exp));
}

export const floorFloat = (value) => decimalAdjust("floor", value, -5);

export function truncAddrs(addrs) {
  return `${addrs.substring(0, 5)}..${addrs.substring(addrs.length - 3)}`;
}

export function displayPrettyStr(input, maxChars = 12) {
  if (!input) {
    return "";
  }
  if (input.length > maxChars) {
    return input.substring(0, maxChars) + "...";
  }
  return input;
}

export function displayPrettyTx(txHash) {
  return `${txHash.substring(2, 8)}`;
}

export function displayPrettyNumber(number, shortened = false) {
  if (!number) {
    return "0";
  }

  const fractionDigits = shortened ? 0 : 2;

  try {
    const number1 = parseFloat(number);
    if (number1 > 1000000000.0) {
      return `${(number1 / 1000000000.0).toFixed(fractionDigits)}B`;
    }
    if (number1 > 1000000.0) {
      return `${(number1 / 1000000.0).toFixed(fractionDigits)}M`;
    }
    if (number1 > 1000.0) {
      return `${(number1 / 1000.0).toFixed(0)}K`;
    }
    return `${number1.toFixed(fractionDigits)}`;
  } catch (e) {
    console.log(`unable to parse ${number} -- please check!!!`);
    return "0";
  }
}

export function toDecimalPlaces(input) {
  const num = Math.abs(input);
  if (num === 0) {
    return 0.0;
  }
  const digits = toDecimalPlaceDigits(input);
  return input?.toFixed(digits);
}

export function toDecimalPlaceDigits(input) {
  const num = Math.abs(input);
  if (num === 0) {
    return 2;
  }
  if (num > 1.1) {
    return 2;
  }
  if (num > 0.1) {
    return 3;
  }
  if (num > 0.01) {
    return 4;
  }
  if (num > 0.001) {
    return 5;
  }
  if (num > 0.0001) {
    return 6;
  }
  return 13;
}

export function useLocalStorage(key, initialValue = null) {
  const [storedValue, setStoredValue] = useState(() => {
    try {
      const item = window.localStorage.getItem(key);
      const value = item ? JSON.parse(item) : initialValue;
      return value;
    } catch (err) {
      console.error(err);
      return initialValue;
    }
  });

  const setValue = (value) => {
    try {
      if (value == null) {
        window.localStorage.removeItem(key);
      } else {
        window.localStorage.setItem(key, JSON.stringify(value));
      }
      setStoredValue(value);
    } catch (err) {
      console.error(err);
    }
  };

  return [storedValue, setValue];
}

export function getGainPctStr(pct, includeParenthsis = false) {
  // eslint-disable-next-line no-nested-ternary
  if (!pct || parseFloat(pct) <= -100) {
    return includeParenthsis ? "" : "--";
  }
  return includeParenthsis ? `(${pct.toFixed(2)}%)` : `${pct.toFixed(2)}%`;
}

export function getErrorFromCode(errCode) {
  switch (errCode) {
    case "ADDRESS_MISMATCH":
      return "Whoops -- your address does not match the address that signed the message.";
    case "NO_TOKEN_IN_WALLET":
      return "Whoops -- you don't have any VISION tokens on this account. Please acquire VISION tokens or connect an account with VISION tokens.";
    case "SERVER_BUSY_FREE_MEMBER":
      return "Whoops! Server is busy. Please try again or become a Pro member to get priority access.";
    case "SERVER_ERROR":
      return "Whoops! Unable to talk to server at the moment. Please try again or alert an admin in our Discord.";
    case "NOT_ENOUGH_VISION":
      return "Whoops - You don't have enough VISION tokens to track this portfolio. Please ensure you have 1 VISION token per $100 USD tracked.";
    case "NOT_EOA_ADDRS":
      return "Whoops - you have entered a contract address. This is currently not supported at the moment.";
    case "NO_ENS_YET":
      return "Whoops - we don't support ENS lookup (yet).";
    case "ACCESS_TOKEN_EXPIRED":
      return "Whoops - you have been logged out. Please sign in again.";
    default:
      return "General Error. Please contact support.";
    // code block
  }
}

export function processChartData(arrayOfValues) {
  const result = [];
  arrayOfValues.map((pair, index) => {
    const impLossUsd = pair.avg_period_daily_imp_loss_usd || 0;
    const feeCollectedUsd = pair.avg_period_daily_fees.toFixed(2);
    const inceptionAverageFeesApy = pair.fee_apys["inception"] || 0;
    const age = pair.pool_age_days;

    result.push({
      keyField: index + 1,
      market: pair.pool_provider_name,
      name: pair.name,
      address: pair.pool_address,
      age,
      idToken0: pair.prices[0].address,
      idToken1: pair.prices[1].address,
      volume_24h_usd: pair.avg_period_daily_volume_usd,
      pool_reserve_usd: pair.avg_period_reserve_usd,
      imp_loss_24h: parseFloat(impLossUsd.toFixed(2)),
      fee_collected_usd: parseFloat(feeCollectedUsd),
      inception_average_fees_apy: parseFloat(
        inceptionAverageFeesApy.toFixed(2)
      ),
      average_apys_7d: parseFloat(pair.fee_apys["7d"]),
      average_apys_14d: pair.fee_apys["14d"],
      average_apys_30d: pair.fee_apys["30d"],
      more_info: pair,
    });
  });
  return result;
}

export function poolAsMap(arrayOfValues) {
  const map = {};
  arrayOfValues.map((pair) => {
    map[pair.pool_address] = pair;
  });
  return map;
}

export function checkSavedAddresses(savedAddresses) {
  const result = [];
  savedAddresses.map((addrs, index) =>
    addrs.address
      ? result.push(addrs)
      : result.push({
          address: addrs,
          displayName: addrs,
        })
  );
  return result;
}

export function capitalize(string) {
  if (!string) {
    return "";
  }
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export function shorten(string) {
  return string.length > 13 ? `${string.slice(0, 10)}...` : string;
}

export function selectDataForCsv(data) {
  const csv = [];
  data.map((pool) => {
    csv.push({
      exchange: pool.poolProviderName,
      name: pool.name,
      address: pool.address,
      "Total Lp Tokens": pool.totalLpTokens,
      "Lp Token Usd Price": pool.lpTokenUsdPrice,
      "Total Value Usd": pool.totalValueUsd,
      "Net Gain Usd": pool.netGainUsd,
      "Total Fee Usd": pool.totalFeeUsd,
      "Net Gain Pct": pool.netGainPct,
      "Owned Lp Tokens Pct": pool.ownedLpTokensPct,
      "Mint Burnt Ledger Lp Tokens": pool.mintBurntLedgerLpTokens,
      "Current Owned Lp Tokens": pool.currentOwnedLpTokens,
      "Initial Capital Value Usd": pool.initialCapitalValueUsd,
    });
  });
  return csv;
}

export function hasSentToFarmingContract(entry) {
  return (
    floorFloat(entry.mintBurntLedgerLpTokens) -
      floorFloat(entry.currentOwnedLpTokens) !==
    0
  );
}

export async function fetchEthPriceFor(market) {
  market = market.toLowerCase();
  let ethPrice = 0;
  let result = 0;
  try {
    if (market === "uniswap") {
      result = await uniswapClient.query({
        query: ETH_PRICE(),
        fetchPolicy: "cache-first",
      });
      ethPrice = result?.data?.bundles[0]?.ethPrice;
    }
  } catch (e) {
    console.log(e);
  }

  return [ethPrice];
}

export function renderEmojis(val) {
  const sads = [
    "😢",
    "😥",
    "😭",
    "😿",
    "😞",
    "☹️",
    "😕",
    "🥵",
    "😰",
    "😩",
    "😧",
    "😨",
    "🤢",
    "🤮",
    "😵",
  ];
  const happies = ["😺", "🤗", "😀", "😃", "☺️", "🤑️", "😇", "🥰", "😍", "🤩"];
  if (val < 0) {
    const sad = sads[Math.abs(parseInt(val, 10)) % sads.length];
    return `${sad}`;
  }
  if (val > 0) {
    const happy = happies[parseInt(val, 10) % happies.length];
    return `${happy}`;
  }

  return null;
}

export function escapeTooltip(tooltip) {
  return tooltip.replace(/[$+]/g, "").replace(/ /g, "");
}

export const capitalizeFirstChar = (str) =>
  str.charAt(0).toUpperCase() + str.substring(1);

export function getLinkForPool(market, addrs) {
  const poolProviderName = capitalize(market);
  switch (poolProviderName) {
    case "Uniswap":
      return `https://uniswap.info/pair/${addrs}`;
    case "Balancer":
      return `https://pools.balancer.exchange/#/pool/${addrs}/`;
    case "Sushiswap":
      return `https://sushiswap.vision/pair/${addrs}`;
    case "Oneinch":
      return `https://1inch.exchange/#/dao/pools/${addrs}/governance`;
    case "VALUE":
      return `https://etherscan.io/address/${addrs}`;
    default:
      return `https://etherscan.io/address/${addrs}`;
  }
}
