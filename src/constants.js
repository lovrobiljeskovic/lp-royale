export const INFURA_ID = "86cffd045a054fccac3e30459b7dd455"; // '8684e0570b104c619193fdbc6dd32964'
export const LOGIN_URI = "/users/login";
export const CLASSIFY_FARM_TX_ACTION = "/farming/tx/classify";
export const CONFIRM_CLASSIFY_FARM_TX_ACTION = "/farming/tx/classify/confirm";
export const DELETE_CLASSIFY_FARM_TX_ACTION = "/farming/tx/classify/delete";
export const DELETE_ALL_CLASSIFY_FARM_TXS_ACTION = "/farming/tx/classify/delete_all";
export const INCLUDE_TX_INCLUDE = "/farming/tx/include";
export const EXCLUDE_TX_INCLUDE = "/farming/tx/exclude";

export const BLOCK_DIFFERENCE_THRESHOLD = 30;

export const API_URL = "https://api.apy.vision";
// export const API_URL = 'http://localhost:3000';

export const UNISWAP_INFO_API_URL = "https://stats.apy.vision";
// export const UNISWAP_INFO_API_URL = 'http://localhost:3000';

export const FACTORY_ADDRESS = "0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f";
export const BUNDLE_ID = "1";

export const timeframeOptions = {
  WEEK: "1 week",
  MONTH: "1 month",
  // THREE_MONTHS: '3 months',
  // YEAR: '1 year',
  ALL_TIME: "All time",
};

export const GENERAL_ERROR = "Whoops - there's been an error. Please try again or contact us on Discord.";

export const AMPLITUDE_API_KEY = "a7a5d0285f74fd3d2f854ba50d393a0c";
