import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { Input, Card, CardBody, Label, FormGroup } from "reactstrap";
import lproyale from "../assets/images/lp-royale-medium.svg";
import pattern from "../assets/images/pattern.svg";
import {
  StyledButton,
  StyledInput,
  StyledSelect,
} from "../components/ButtonsAndInputs";

const Container = styled.div`
  background: url(${pattern}), #0fb1d8;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;
  min-height: 100vh;
  .card {
    min-width: 464px;
    border-radius: 24px;
    @media only screen and (max-width: 600px) {
      min-width: 0px;
      margin-left: 1rem;
      margin-right: 1rem;
    }
  }
  img {
    position: absolute;
    top: -50px;
    left: 126px;
    @media only screen and (max-width: 600px) {
      left: 62px;
    }
  }
  span {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 900;
    font-size: 14px;
    color: #9fbacf !important;
  }
  h2 {
    font-family: "MachineGunk";
    font-style: normal;
    font-weight: normal;
    font-size: 28px;
    text-align: start;
    padding-top: 56px;
    padding-bottom: 12px;
  }
`;

const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: start;
  p {
    margin-bottom: 4px !important;
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 900;
    font-size: 14px;
    color: black !important;
  }
  input {
    margin-bottom: 14px;
  }
`;

const CheckboxContainer = styled.div`
  .form-group {
    margin-bottom: 12px;
  }
  .custom-control-input:checked ~ .custom-control-label::before {
    background-color: #5652cc;
    border-color: #5652cc;
  }
  .custom-control-label::after {
    top: 0.01rem;
    left: -1.75rem;
    width: 1.5rem;
    height: 1.5rem;
  }
`;

export const SignUp = () => {
  return (
    <Container>
      <Card>
        <img src={lproyale} />
        <CardBody style={{padding: 32}}>
          <h2>Signup</h2>
          <InputContainer>
            <p>Email</p>
            <StyledInput type="text" placeholder="Your email" />
            <p>Username</p>
            <StyledInput type="text" placeholder="Your username" />
            <p>LP expertise level</p>
            <StyledSelect type="select" name="select" id="levelSelect">
              <option value="">Pick a level...</option>
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </StyledSelect>
          </InputContainer>
          <div
            className="d-flex"
            style={{
              alignItems: "center",
              justifyContent: "space-between",
              marginTop: 16,
              marginBottom: 76,
            }}
          >
            <span>We’ll send you a link to login</span>
            <StyledButton primary size="xl">
              Sign Up
            </StyledButton>
          </div>
          <CheckboxContainer
            className="d-flex custom-control custom-checkbox"
            style={{
              alignItems: "flex-start",
              flexDirection: "column",
            }}
          >
            <FormGroup>
              <Input
                type="checkbox"
                className="custom-control-input"
                id="royaleCheck"
              />
              <Label className="custom-control-label" for="royaleCheck">
                I agree to receive email updates from LP Royale
              </Label>
            </FormGroup>
            <FormGroup>
              <Input
                type="checkbox"
                className="custom-control-input"
                id="uniswapCheck"
              />
              <Label className="custom-control-label" for="uniswapCheck">
                I agree to receive email updates from Uniswap
              </Label>
            </FormGroup>
          </CheckboxContainer>
        </CardBody>
      </Card>
    </Container>
  );
};
