import React, { useState } from "react";
import { ArrowDownRight, ArrowUpRight } from "react-feather";
import DataTable, { createTheme } from "react-data-table-component";
import DoubleTokenLogo from "./DoubleTokenLogo";
import { Col, Row } from "reactstrap";
import styled from "styled-components";
import { isMobile } from "react-device-detect";
import { Modal } from "reactstrap";
import "../Modal.css";
import {
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";
import {
  StyledButton,
  StyledInput,
  StyledPrependInput,
} from "../components/ButtonsAndInputs";

createTheme("custom", {
  background: {
    default: "$primary-color",
  },
});

const customStyles = {
  rows: {
    style: {
      height: "80px", // override the row height
      textAlign: "center",
      borderTop: "1px solid #9FBACF",
    },
  },
  headCells: {
    style: {
      paddingTop: "20px",
      paddingBottom: "20px",
      textAlign: "center",
      verticalAlign: "middle",
      fontFamily: "Nunito Sans, sans-serif",
      fontWeight: 800,
      fontSize: "14px",
      textTransform: "uppercase",
      justifyContent: isMobile && "center",
      flexDirection: isMobile && "column",
    },
    activeSortStyle: {
      color: "$primary-color",
      "&:focus": {
        outline: "none",
      },
      "&:hover:not(:focus)": {
        color: "$primary-color",
      },
    },
    inactiveSortStyle: {
      "&:focus": {
        outline: "none",
        color: "$primary-color",
      },
      "&:hover": {
        color: "$primary-color",
      },
    },
  },
  table: {
    style: {
      borderCollapse: "collapse",
      boxShadow: "0px 1px 0px #9FBACF",
      borderRadius: "23px 23px 0px 0px",
      borderTop: "1px solid #9FBACF",
      borderLeft: "1px solid #9FBACF",
      borderRight: "1px solid #9FBACF",
      borderBottom: "1px solid #9FBACF",
    },
  },
  cells: {
    style: {
      verticalAlign: "middle",
      fontFamily: "Nunito Sans, sans-serif",
      fontWeight: 900,
    },
  },
  pagination: {
    pageButtonsStyle: {
      fill: "darkgray",
    },
  },
};

const ContentContainer = styled.div`
  display: flex;
`;

const Left = styled.div`
  margin-top: 12px;
  margin-bottom: 12px;
  margin-left: 12px;
  margin-right: 14px;
  border-radius: 24px;
  background-color: #f3f8fc;
  width: 45%;
  padding-top: 24px;
  padding-left: 16px;
  padding-right: 16px;
  padding-bottom: 12px;
  display: flex;
  flex-direction: column;
  p {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 900;
    font-size: 12px;
    color: #042b4e;
    text-transform: uppercase;
  }
  h1 {
    font-family: "MachineGunk";
    font-style: normal;
    font-weight: normal;
    font-size: 28px;
    color: #042b4e;
    text-align: center;
  }
  .input-group-prepend {
    span {
      background: #042b4e;
      color: white;
    }
  }
  .input-group-text {
    border-top-left-radius: 8px;
    border-bottom-left-radius: 8px;
    border-top-right-radius: 8px;
    border-bottom-right-radius: 8px;
    border: none !important;
  }
  .form-control {
    background: linear-gradient(180deg, #d6dcec 0%, #ffffff 100%);
    border: 1px solid #042b4e;
  }
  .form-group {
    width: 100%;
    margin-bottom: 8px;
  }
  .row {
    margin-left: 0px;
    margin-right: 0px;
  }
  @media only screen and (max-width: 600px) {
    width: 50%;
  }
`;
const Right = styled.div`
  margin-top: 12px;
  margin-bottom: 12px;
  margin-right: 12px;
  border-radius: 24px;
  background-color: #f3f8fc;
  display: flex;
  padding-top: 24px;
  padding-left: 16px;
  padding-right: 16px;
  padding-bottom: 12px;
  width: 55%;
  .no-amount {
    width: 100%;
    text-align: center;
    align-self: center;
  }
  span {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 800;
    font-size: 14px;
    color: #042b4e;
  }

  @media only screen and (max-width: 600px) {
    width: 50%;
  }
`;

const LogoContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  font-family: "Nunito Sans", sans-serif;
  font-style: normal;
  font-weight: 900;
  color: #042b4e;
  margin-top: 42px;
  margin-bottom: 62px;
  p {
    margin-bottom: 0px;
    margin-top: 8px;
    font-size: 22px;
  }
  > * {
    &:first-child {
      left: -20px;
    }
  }
`;

const TransactionDetails = styled.div`
  h2 {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 900;
    font-size: 12px;
    color: #9fbacf;
  }
`;

const Pagination = styled.div`
  border-bottom-left-radius: 24px;
  border-bottom-right-radius: 24px;
  border: "1px solid #9FBACF";
  min-height: 70px;
`;

const PaginationButton = styled.button`
  align-items: center;
  width: 100%;
  padding-top: 14px;
  padding-bottom: 14px;
  border: 1px solid #9fbacf;
  box-sizing: border-box;
  border-bottom-left-radius: 24px;
  border-bottom-right-radius: 24px;
  font-size: 18px;
  font-weight: 900;
  font-family: "Nunito Sans", sans-serif;
  color: white;
  background: linear-gradient(
    37.78deg,
    #5d45b3 -2.02%,
    #6b59d7 47.31%,
    #713ae7 47.32%,
    #49a8ff 103.26%
  );
  @media only screen and (max-width: 600px) {
    width: 100%;
  }
`;

export const YourPoolsTable = (props) => {
  const [modal, setModal] = useState(false);
  const [selectedPair, setSelectedPair] = useState(null);
  const [amount, setAmount] = useState(null);
  const toggle = (pair) => {
    if (selectedPair) {
      setSelectedPair(null);
    } else {
      setSelectedPair(pair);
    }
    setModal(!modal);
  };

  const handleShowMore = () => {
    props.setShowMore(true);
  };

  const idk = () => {
    return (
      <div>
        {!props.showMore ? (
          <PaginationButton onClick={() => handleShowMore()}>
            Show More
          </PaginationButton>
        ) : (
          <Pagination />
        )}
      </div>
    );
  };
  const renderLastApySpan = (apyOverall, apyForPeriod) => {
    const isGain =
      apyOverall < apyForPeriod ? (
        <ArrowUpRight
          style={{ color: "#23B676" }}
          className="warning-icon"
          size={16}
        />
      ) : (
        <ArrowDownRight
          style={{ color: "#FF495C" }}
          className="warning-icon"
          size={16}
        />
      );
    return (
      <>
        <span>{apyForPeriod.toFixed(2)}%</span>
        {isGain}
      </>
    );
  };

  const columns = [
    {
      name: "Pool Name",
      selector: "name",
      sortable: true,
      width: "148px",
      center: true,
      cell: (row) => (
        <div>
          <b>
            <div style={{ marginLeft: 6, marginBottom: 6 }}>
              <DoubleTokenLogo size={32} a0={""} a1={""} />
            </div>
            <p style={{ marginBottom: 0, marginRight: 10 }}>{row.poolName}</p>
          </b>
        </div>
      ),
    },
    {
      name: "Tokens entered → exited",
      selector: "volume_24h_usd",
      sortable: true,
      center: true,
      cell: (row) => (
        <div
          className="d-flex justify-content-around"
          style={{ width: "100%" }}
        >
          <b className="d-flex align-items-center">
            <h1
              style={{
                fontWeight: 900,
                fontSize: 18,
                marginBottom: 0,
                marginRight: 8,
              }}
            >
              {row.tokensEntered}
            </h1>
            <h2 style={{ fontWeight: 900, fontSize: 14, marginBottom: 0 }}>
              {row.tokensEnteredName}
            </h2>
          </b>
          <b className="d-flex align-items-center">
            <h1
              style={{
                fontWeight: 900,
                fontSize: 18,
                marginBottom: 0,
                marginRight: 8,
              }}
            >
              {row.tokensEntered}
            </h1>
            <h2 style={{ fontWeight: 900, fontSize: 14, marginBottom: 0 }}>
              {row.tokensEnteredName}
            </h2>
          </b>
        </div>
      ),
    },
    {
      name: "Liquidity pool gains",
      selector: "name",
      sortable: true,
      center: true,
      cell: (row) => (
        <b style={{ fontSize: 18, fontWeight: 900 }}>
          {row.liquidityPoolGains}
        </b>
      ),
    },
    {
      name: "",
      sortable: false,
      width: "208px",
      center: true,
      cell: (row) => (
        <div className="d-flex flex-column">
          <StyledButton primary size="md" onClick={() => toggle(row)}>
            Manage Liquidity
          </StyledButton>
          <div className="d-flex">
            <StyledButton size="sm">Pool Stats</StyledButton>
            <StyledButton size="sm">More Info</StyledButton>
          </div>
        </div>
      ),
    },
  ];

  return (
    <div
      className="responsive"
      id="top-pools-table"
      style={{ width: "100%", marginBottom: 140, overflowX: "auto" }}
    >
      <DataTable
        pagination
        paginationComponent={idk}
        columns={columns}
        data={props.showMore ? props.tableData : props.tableData.slice(0, 1)}
        customStyles={customStyles}
        theme="custom"
        responsive={false}
        noHeader
        style={{ flexDirection: "column" }}
      />
      <Modal
        isOpen={modal}
        toggle={() => toggle()}
        modalClassName="custom-modal"
      >
        <ContentContainer>
          <Left>
            <h1>Add liquidity to:</h1>
            {selectedPair && (
              <LogoContainer>
                <DoubleTokenLogo
                  size={64}
                  a0={selectedPair.idToken0}
                  a1={selectedPair.idToken1}
                />
                <p>{selectedPair.name}</p>
              </LogoContainer>
            )}
            <p>Available funds: $100,000</p>
            <Form
              onSubmit={(e) => {
                e.preventDefault();
                console.log("todo");
              }}
              className="d-flex align-items-center justify-content-around"
            >
              <FormGroup row>
                <InputGroup style={{ flexWrap: "nowrap" }}>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>$</InputGroupText>
                  </InputGroupAddon>
                  <StyledPrependInput
                    onChange={(e) => setAmount(e.target.value)}
                    value={amount}
                    type="text"
                    placeholder="Enter amount ..."
                  />
                </InputGroup>
              </FormGroup>
            </Form>
            <StyledButton size="lg">Confirm</StyledButton>
          </Left>
          <Right>
            {!amount ? (
              <div className="no-amount">
                <span>
                  Enter an amount to view <br /> the transaction costs details
                </span>
              </div>
            ) : (
              <TransactionDetails>
                <h2>TRANSACTION DETAILS</h2>
              </TransactionDetails>
            )}
          </Right>
        </ContentContainer>
      </Modal>
    </div>
  );
};
