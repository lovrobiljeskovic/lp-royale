import React, { useEffect, useState } from "react";
import { Card, CardHeader, Col, Container, Row, Table } from "reactstrap";
import {
  displayPrettyNumber,
  getPoolIcon,
  toDecimalPlaces,
} from "../helpers/util";
import {
  Bar,
  BarChart,
  CartesianGrid,
  Legend,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
  ReferenceLine,
} from "recharts";
import { Link } from "react-router-dom";
import GoProOrCell from "./GoProOrCell";
import styled from "styled-components";
import { isMobile } from "react-device-detect";

const BarContainer = styled.div`
  .recharts-cartesian-axis-tick {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 800;
    font-size: 12px;
    text {
      fill: #042b4e;
    }
  }
  .recharts-cartesian-axis-tick-value {
    fill: #042b4e;
  }
  .recharts-legend-wrapper {
    bottom: -30px !important;
  }
  .recharts-default-legend {
    text-align: center !important;
    @media only screen and (max-width: 600px) {
      text-align: end !important;
      margin-left: 0px !important;
    }
  }
`;

const CardContainer = styled.div`
  .card {
    margin-bottom: 60px;
    background: #ffffff;
    border: 1px solid #9fbacf;
    box-sizing: border-box;
    box-shadow: 0px 4px 0px rgba(4, 43, 78, 0.14);
    border-radius: 0px 0px 24px 24px;
    /* .tableresponsive {
      ::-webkit-scrollbar {
        height: 5px;
      }

      ::-webkit-scrollbar-track {
        box-shadow: inset 0 0 5px grey;
        border-radius: 10px;
      }

      ::-webkit-scrollbar-thumb {
        background: black;
        border-radius: 10px;
      }

      ::-webkit-scrollbar-thumb:hover {
        background: rgb(54, 56, 58);
      }
    } */
  }
  .table {
    margin-bottom: 0px !important;
  }
  .table td {
    border-top: 1px solid #9fbacf;
    font-size: 14px;
    text-align: center;
    font-family: "Nunito Sans", sans-serif;
    font-weight: 900;
    word-wrap: break-word;
    color: #1b3155;
    padding-top: 20px !important;
    padding-bottom: 20px !important;
    padding-left: 20px !important;
    padding-right: 20px !important;
    text-align: center;
    vertical-align: middle;
  }
  .table thead th {
    border-bottom: 1px solid #9fbacf;
    font-size: 12px;
    max-width: 180px;
    text-align: center;
    text-transform: uppercase;
    color: #1b3155;
    font-family: "Nunito Sans", sans-serif;
  }
  .table th {
    border-top: none;
    padding-top: 20px !important;
    padding-bottom: 20px !important;
    padding-left: 20px !important;
    padding-right: 20px !important;
  }
`;

export default function (props) {
  const poolInfo = props.poolInfo;
  const currentPrices = props.currentPrices;
  const [barChartData, setBarChartData] = useState();
  const nameToken0 = poolInfo && poolInfo.prices[0].symbol;
  const nameToken1 = poolInfo && poolInfo.prices[1].symbol;

  function getValuesFor(period) {
    let liqPeriod = period === "inception" ? period : period.slice(0, -4);
    let hodlToken0Val = (1000 / poolInfo.prices[0][period]) * currentPrices[0];
    let hodlToken1Val = (1000 / poolInfo.prices[1][period]) * currentPrices[1];
    let hodl5050Val =
      (500 / poolInfo.prices[0][period]) * currentPrices[0] +
      (500 / poolInfo.prices[1][period]) * currentPrices[1];
    let liquidityPoolVal =
      1000 * (1 + poolInfo.hodl_minus_il_return_pcts[liqPeriod] / 100);

    return {
      token0: hodlToken0Val,
      token1: hodlToken1Val,
      token01: hodl5050Val,
      liqPoolVal: liquidityPoolVal,
    };
  }

  function convertToChartData(entry) {
    const results = [];
    let hodl = {};
    if (currentPrices) {
      if (entry.pool_age_days >= 30) {
        hodl = getValuesFor("inception");
        results.push({
          name: "Since Inception*",
          [nameToken0]: hodl.token0,
          [nameToken1]: hodl.token1,
          "50% 50%": hodl.token01,
          "Curr Liq Pool Value": hodl.liqPoolVal,
        });
      }
      if (entry.pool_age_days >= 30) {
        hodl = getValuesFor("30d_usd");
        results.push({
          name: "30D Ago",
          [nameToken0]: hodl.token0,
          [nameToken1]: hodl.token1,
          "50% 50%": hodl.token01,
          "Curr Liq Pool Value": hodl.liqPoolVal,
        });
      }

      if (entry.pool_age_days >= 14) {
        hodl = getValuesFor("14d_usd");
        results.push({
          name: "14D Ago",
          [nameToken0]: hodl.token0,
          [nameToken1]: hodl.token1,
          "50% 50%": hodl.token01,
          "Curr Liq Pool Value": hodl.liqPoolVal,
        });
      }
      if (entry.pool_age_days >= 7) {
        hodl = getValuesFor("7d_usd");
        results.push({
          name: "7D Ago",
          [nameToken0]: hodl.token0,
          [nameToken1]: hodl.token1,
          "50% 50%": hodl.token01,
          "Curr Liq Pool Value": hodl.liqPoolVal,
        });
      }
      hodl = getValuesFor("1d_usd");
      results.push({
        name: "Yesterday",
        [nameToken0]: hodl.token0,
        [nameToken1]: hodl.token1,
        "50% 50%": hodl.token01,
        "Curr Liq Pool Value": hodl.liqPoolVal,
      });
    }

    return results;
  }

  useEffect(() => {
    const convertData = () => {
      if (poolInfo) {
        setBarChartData(convertToChartData(poolInfo));
      }
    };
    convertData();
  }, [currentPrices]);

  const CustomizedAxisTick = (props) => {
    const { x, y, payload } = props;

    return (
      <g transform={`translate(${x},${y})`}>
        <text
          x={0}
          y={0}
          dy={16}
          textAnchor={isMobile ? "end" : "middle"}
          verticalAnchor="middle"
          fill="#666"
          transform={isMobile ? "rotate(-25)" : "rotate(0)"}
        >
          {payload.value}
        </text>
      </g>
    );
  };

  function renderLiquidityGainsChart() {
    function getMinBarData() {
      const number = Math.min(
        ...barChartData.map((entry) => entry["100% Token0"])
      );
      const number2 = Math.min(
        ...barChartData.map((entry) => entry["100% Token1"])
      );
      const number3 = Math.min(
        ...barChartData.map((entry) => entry["50% 50%"])
      );
      const number4 = Math.min(
        ...barChartData.map((entry) => entry["Curr Liq Pool Value"])
      );
      return Math.min(number, number2, number3, number4);
    }

    function getMaxBarData() {
      const number = Math.max(
        ...barChartData.map((entry) => entry["100% Token0"])
      );
      const number2 = Math.max(
        ...barChartData.map((entry) => entry["100% Token1"])
      );
      const number3 = Math.max(
        ...barChartData.map((entry) => entry["50% 50%"])
      );
      const number4 = Math.max(
        ...barChartData.map((entry) => entry["Curr Liq Pool Value"])
      );
      return Math.max(number, number2, number3, number4);
    }

    const CustomTooltip = ({ active, payload, label }) => {
      if (active && payload) {
        return (
          <div className="chart-hover">
            <p
              style={{ color: "#0FB1D8" }}
            >{`${nameToken0}: $${payload[0]?.value.toFixed(2)} USD`}</p>
            <p
              style={{ color: "#9FBACF" }}
            >{`${nameToken1}: $${payload[1]?.value.toFixed(2)} USD`}</p>
            <p
              style={{ color: "#5652CC" }}
            >{`50% 50%: $${payload[2]?.value.toFixed(2)} USD`}</p>
            <p
              style={{ color: "#23B676" }}
            >{`Current Liq Pool Value: $${payload[3]?.value.toFixed(
              2
            )} USD`}</p>
          </div>
        );
      }
      return null;
    };

    return (
      <>
        {barChartData ? (
          <BarContainer style={{ width: "100%", height: 350 }} className="mb-5">
            <ResponsiveContainer>
              <BarChart
                height={350}
                data={barChartData}
                margin={{
                  top: 20,
                  right: 20,
                  left: 20,
                  bottom: 5,
                }}
              >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis
                  dataKey="name"
                  angle={-25}
                  interval={0}
                  dx={-20}
                  dy={10}
                  height={45}
                  tickLine={false}
                  tick={<CustomizedAxisTick />}
                />
                <YAxis
                  tickFormatter={(tick) => `$${tick}`}
                  padding={{ top: 20, bottom: 20 }}
                  // domain={[getMinBarData(), getMaxBarData()]}
                  domain={[0, 3500]}
                  allowDataOverflow={true}
                />
                <Tooltip content={<CustomTooltip />} />
                <Legend verticalAlign="bottom" />

                <Bar dataKey={nameToken0} fill="#0FB1D8" />
                <Bar dataKey={nameToken1} fill="#9FBACF" />
                <Bar dataKey="50% 50%" fill="#5652CC" />
                <Bar dataKey="Curr Liq Pool Value" fill="#23B676" />
                <ReferenceLine y={1000} strokeWidth="3" stroke="#042B4E" />
              </BarChart>
            </ResponsiveContainer>
          </BarContainer>
        ) : null}
      </>
    );
  }

  function renderRow(
    t0Sym,
    t1Sym,
    dateLabel,
    priceToken0,
    priceToken1,
    hodlMinusIlReturnPct
  ) {
    const liquidityPoolVal = 1000 * (1 + hodlMinusIlReturnPct / 100);
    const hodlToken0Val = (1000 / priceToken0) * currentPrices[0];
    const hodlToken1Val = (1000 / priceToken1) * currentPrices[1];
    const hodl5050Val =
      (500 / priceToken0) * currentPrices[0] +
      (500 / priceToken1) * currentPrices[1];
    const is_pro =
      hodlMinusIlReturnPct !== -1 && priceToken0 !== -1 && priceToken1 !== -1;

    return priceToken0 === 0 || priceToken1 === 0 ? null : (
      <tr>
        <td scope="row">{dateLabel}</td>
        <td>
          <GoProOrCell pro={is_pro}>
            ${toDecimalPlaces(priceToken0)}
          </GoProOrCell>
        </td>
        <td>
          <GoProOrCell pro={is_pro}>
            ${toDecimalPlaces(priceToken1)}
          </GoProOrCell>
        </td>
        <td>
          <GoProOrCell pro={is_pro}>
            {toDecimalPlaces(1000 / priceToken0)} {t0Sym}
          </GoProOrCell>
        </td>
        <td
          className={
            hodlToken0Val > 1000
              ? "font-success bordered"
              : "font-danger bordered"
          }
        >
          <GoProOrCell pro={is_pro}>
            ${toDecimalPlaces(hodlToken0Val)}
          </GoProOrCell>
        </td>
        <td>
          <GoProOrCell pro={is_pro}>
            {toDecimalPlaces(1000 / priceToken1)} {t1Sym}
          </GoProOrCell>
        </td>
        <td
          className={
            hodlToken1Val > 1000
              ? "font-success bordered"
              : "font-danger bordered"
          }
        >
          <GoProOrCell pro={is_pro}>
            ${toDecimalPlaces(hodlToken1Val)}
          </GoProOrCell>
        </td>
        <td>
          <GoProOrCell pro={is_pro}>
            {toDecimalPlaces(500 / priceToken0)} {t0Sym} /{" "}
            {toDecimalPlaces(500 / priceToken1)} {t1Sym}
          </GoProOrCell>
        </td>
      </tr>
    );
  }

  if (poolInfo && currentPrices) {
    const t0Sym = poolInfo.prices[0].symbol;
    const t1Sym = poolInfo.prices[1].symbol;
    return (
      <div>
        <CardContainer>
          <Card>
            <div className="card-block row">
              <Col sm="12" lg="12" xl="12">
                {renderLiquidityGainsChart()}
              </Col>
            </div>
          </Card>
        </CardContainer>
        <h6
          style={{
            fontFamily: "Nunito Sans, sans-serif",
            fontSize: 18,
            fontWeight: 900,
            color: "#042B4E",
          }}
        >
          Liquidity Provider Gains vs. Holding The Assets Per $1000 USD
        </h6>
        <p
          style={{
            fontFamily: "Nunito Sans, sans-serif",
            fontSize: 14,
            fontWeight: 900,
            color: "#042B4E",
            marginBottom: 12,
          }}
        >
          This shows your profits and losses for a $1000 investment if you held
          the tokens vs. providing liquidity at various time periods, taking
          into account Impermanent Loss and net market gains for the period.
        </p>
        <CardContainer>
          <Card style={{ borderRadius: 24 }}>
            <div className="tableresponsive" style={{ overflowX: "auto" }}>
              <Table>
                <thead>
                  <tr>
                    <th scope="col"></th>
                    <th scope="col">{t0Sym} price</th>
                    <th scope="col">Assets if 100% {t0Sym}</th>
                    <th scope="col">{t1Sym} price</th>
                    <th scope="col">Assets if 100% 100% {t1Sym}</th>
                    <th scope="col">Initial Assets 50/50 split</th>
                    <th
                      scope="col"
                      className="bordered"
                      style={{ position: "relative" }}
                    >
                      Value if hold
                    </th>
                    <th scope="col">Value in LP</th>
                  </tr>
                </thead>
                <tbody>
                  {poolInfo.pool_age_days >= 1
                    ? renderRow(
                        t0Sym,
                        t1Sym,
                        "Yesterday",
                        poolInfo.prices[0]["1d_usd"],
                        poolInfo.prices[1]["1d_usd"],
                        poolInfo.hodl_minus_il_return_pcts["1d"]
                      )
                    : null}
                  {poolInfo.pool_age_days >= 7
                    ? renderRow(
                        t0Sym,
                        t1Sym,
                        "7 Days Ago",
                        poolInfo.prices[0]["7d_usd"],
                        poolInfo.prices[1]["7d_usd"],
                        poolInfo.hodl_minus_il_return_pcts["7d"]
                      )
                    : null}
                  {poolInfo.pool_age_days >= 14
                    ? renderRow(
                        t0Sym,
                        t1Sym,
                        "14 Days Ago",
                        poolInfo.prices[0]["14d_usd"],
                        poolInfo.prices[1]["14d_usd"],
                        poolInfo.hodl_minus_il_return_pcts["14d"]
                      )
                    : null}
                  {poolInfo.pool_age_days >= 30
                    ? renderRow(
                        t0Sym,
                        t1Sym,
                        "30 Days Ago",
                        poolInfo.prices[0]["30d_usd"],
                        poolInfo.prices[1]["30d_usd"],
                        poolInfo.hodl_minus_il_return_pcts["30d"]
                      )
                    : null}
                  {poolInfo.pool_age_days >= 30
                    ? renderRow(
                        t0Sym,
                        t1Sym,
                        <div>
                          <p style={{ marginBottom: 0, whiteSpace: "nowrap" }}>
                            Pool Inception
                          </p>
                          <span
                            style={{
                              fontSize: 12,
                              fontWeight: 900,
                              color: "#9FBACF",
                            }}
                          >
                            {poolInfo.pool_age_days} Days Ago
                          </span>
                        </div>,
                        poolInfo.prices[0]["inception"],
                        poolInfo.prices[1]["inception"],
                        poolInfo.hodl_minus_il_return_pcts["inception"]
                      )
                    : null}
                </tbody>
              </Table>
            </div>
          </Card>
        </CardContainer>
      </div>
    );
  }
  return null;
}
