import React, { PureComponent, useEffect, useState } from "react";
import { Alert, CardHeader, Col, NavLink, Row } from "reactstrap";
import { displayPrettyNumber } from "../helpers/util";
import {
  Area,
  ReferenceLine,
  Bar,
  Line,
  Brush,
  BarChart,
  CartesianGrid,
  Legend,
  ResponsiveContainer,
  Tooltip,
  LineChart,
  XAxis,
  YAxis,
  ComposedChart,
} from "recharts";
import styled from "styled-components";
import { isMobile } from "react-device-detect";

const BarContainer = styled.div`
  .recharts-cartesian-axis-tick {
    font-family: "Nunito Sans", sans-serif;
    font-style: normal;
    font-weight: 800;
    font-size: 12px;
    text {
      fill: #042b4e;
    }
  }
  .recharts-cartesian-axis-tick-value {
    fill: #042b4e;
  }
  .recharts-legend-wrapper {
    bottom: -30px !important;
  }
  .recharts-default-legend {
    text-align: center !important;
    @media only screen and (max-width: 600px) {
      text-align: end !important;
      margin-left: 0px !important;
    }
  }
`;
export default function (props) {
  const [chartData, setChartData] = useState();
  const poolInfo = props.poolInfo;

  const CustomizedAxisTick = (props) => {
    const { x, y, payload } = props;

    return (
      <g transform={`translate(${x},${y})`}>
        <text
          x={0}
          y={0}
          dy={16}
          textAnchor={isMobile ? "end" : "middle"}
          verticalAnchor="middle"
          fill="#666"
          transform={isMobile ? "rotate(-25)" : "rotate(0)"}
        >
          {payload.value}
        </text>
      </g>
    );
  };

  const CustomTooltip = ({ active, payload, label }) => {
    if (active && payload) {
      return (
        <div className="chart-hover">
          <p
            style={{ color: "#9FBACF" }}
          >{`Avg Transactions: ${payload[0]?.value} Txs`}</p>
          <p
            style={{ color: "#042B4E" }}
          >{`Avg Swap Size: $${displayPrettyNumber(payload[1]?.value)} USD`}</p>
          <p
            style={{ color: "#5652CC" }}
          >{`Median Swap Size: $${payload[2]?.value} USD`}</p>
        </div>
      );
    }
    return null;
  };

  const formatYAxis = (tickItem) => {
    return displayPrettyNumber(tickItem, true);
  };

  class CustomizedLabel extends PureComponent {
    render() {
      const { x, y, stroke, value, transaction } = this.props;
      return (
        <text
          x={x}
          y={y}
          dy={-8}
          fill={stroke}
          fontSize={10}
          textAnchor="middle"
        >
          {transaction ? `${value} Txs` : `$${displayPrettyNumber(value)} USD`}
        </text>
      );
    }
  }

  function convertToChartData(entry) {
    const results = [];

    if (!entry.median_swap_value) {
      return results;
    }

    if (poolInfo.pool_age_days >= 30) {
      results.push({
        name: "Inception",
        "Avg # Txns": entry.avg_txns_size["inception"].toFixed(0),
        "Avg Swap Size": entry.avg_swap_value_usd["inception"].toFixed(2),
        "Median Swap Size": entry.median_swap_value["inception"].toFixed(2),
      });
    }

    if (poolInfo.pool_age_days >= 90) {
      if (
        entry.avg_txns_size["90d"] !== -1 &&
        entry.avg_swap_value_usd["90d"] !== -1
      ) {
        results.push({
          name: "Avg 90D",
          "Avg # Txns": entry.avg_txns_size["90d"].toFixed(0),
          "Avg Swap Size": entry.avg_swap_value_usd["90d"].toFixed(2),
          "Median Swap Size": entry.median_swap_value["90d"].toFixed(2),
        });
      }
    }

    if (poolInfo.pool_age_days >= 60) {
      if (
        entry.avg_txns_size["60d"] !== -1 &&
        entry.avg_swap_value_usd["60d"] !== -1
      ) {
        results.push({
          name: "Avg 60D",
          "Avg # Txns": entry.avg_txns_size["60d"].toFixed(0),
          "Avg Swap Size": entry.avg_swap_value_usd["60d"].toFixed(2),
          "Median Swap Size": entry.median_swap_value["60d"].toFixed(2),
        });
      }
    }

    if (poolInfo.pool_age_days >= 30) {
      results.push({
        name: "Avg 30D",
        "Avg # Txns": entry.avg_txns_size["30d"].toFixed(0),
        "Avg Swap Size": entry.avg_swap_value_usd["30d"].toFixed(2),
        "Median Swap Size": entry.median_swap_value["30d"].toFixed(2),
      });
    }

    if (poolInfo.pool_age_days >= 14) {
      results.push({
        name: "Avg 14D",
        "Avg # Txns": entry.avg_txns_size["14d"].toFixed(0),
        "Avg Swap Size": entry.avg_swap_value_usd["14d"].toFixed(2),
        "Median Swap Size": entry.median_swap_value["14d"].toFixed(2),
      });
    }
    if (poolInfo.pool_age_days >= 7) {
      results.push({
        name: "Avg 7D",
        "Avg # Txns": entry.avg_txns_size["7d"].toFixed(0),
        "Avg Swap Size": entry.avg_swap_value_usd["7d"].toFixed(2),
        "Median Swap Size": entry.median_swap_value["7d"].toFixed(2),
      });
    }

    results.push({
      name: "Yesterday",
      "Avg # Txns": entry.avg_txns_size["1d"].toFixed(0),
      "Avg Swap Size": entry.avg_swap_value_usd["1d"].toFixed(2),
      "Median Swap Size": entry.median_swap_value["1d"].toFixed(2),
    });
    return results;
  }

  useEffect(() => {
    const convertData = () => {
      if (poolInfo) {
        setChartData(convertToChartData(poolInfo));
      }
    };
    convertData();
  }, [poolInfo]);

  function getMinBarData() {
    const number = Math.min(
      ...chartData.map((entry) => entry["Avg Swap Size"])
    );
    const number2 = Math.min(
      ...chartData.map((entry) => entry["Median Swap Size"])
    );
    return Math.min(number, number2);
  }

  function getMaxBarData(dataName) {
    return Math.max(...chartData.map((entry) => entry[dataName]));
  }

  return (
    <>
      {chartData ? (
        <BarContainer style={{ width: "100%", height: 350 }} className="mb-5">
          <ResponsiveContainer width="99%">
            <ComposedChart
              height={400}
              data={chartData}
              margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
            >
              <CartesianGrid strokeDasharray="3 3" />

              <XAxis
                dataKey="name"
                interval={0}
                padding={{ left: 15, right: 15 }}
                height={45}
                dx={-20}
                dy={10}
                tickLine={false}
                tick={<CustomizedAxisTick />}
              />
              <YAxis
                yAxisId="left"
                padding={{ top: 20, bottom: 20 }}
                domain={[
                  (dataMin) => getMinBarData(),
                  (dataMax) => getMaxBarData("Avg Swap Size"),
                ]}
                allowDataOverflow={true}
                tickFormatter={formatYAxis}
              />
              <YAxis
                yAxisId="bar"
                allowDataOverflow={true}
                orientation="left"
                hide={true}
              />
              <Tooltip content={<CustomTooltip />} />
              <Legend verticalAlign="bottom" />
              <Bar
                yAxisId="bar"
                dataKey="Avg # Txns"
                barSize={20}
                fill="#9FBACF"
                minPointSize={3}
              ></Bar>
              <Line
                yAxisId="left"
                type="monotone"
                dataKey="Avg Swap Size"
                stroke="#042B4E"
                strokeWidth={2}
              />
              <Line
                yAxisId="left"
                type="monotone"
                dataKey="Median Swap Size"
                stroke="#5652CC"
                strokeWidth={2}
              />
            </ComposedChart>
          </ResponsiveContainer>
        </BarContainer>
      ) : null}
    </>
  );
}
