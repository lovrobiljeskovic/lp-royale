import React from "react";
import { Link } from "react-router-dom";

export default function GoProOrCell(props) {
  return (
    <>{props.pro ? props.children : <Link to="/membership">[go pro]</Link>}</>
  );
}
