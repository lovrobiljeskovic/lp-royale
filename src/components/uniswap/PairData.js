import {
  ETH_PRICE,
  PAIR_DATA,
  PAIR_GRAPH_PRICES_UNISWAP,
  PAIRS_BULK,
  PAIRS_HISTORICAL_BULK,
  UNISWAP_LP_DATA_SINCE_TIMESTAMP,
  UNISWAP_LP_PRICE,
  UNISWAP_NEWEST_POOLS,
} from "./queries";
import { uniswapClient } from "./client";
import {
  get2DayPercentChange,
  getBlocksFromTimestamps,
  getMetricsForPositionWindowCustom,
  getPercentChange,
  getTimestampsForChanges,
} from "./utils/utils";

function parseData(data, oneDayData, twoDayData, threeDayData, ethPrice, oneDayBlock) {
  // get volume changes
  const [oneDayVolumeUSD, volumeChangeUSD, twoDayVolumeUSD] = get2DayPercentChange(
    data?.volumeUSD,
    oneDayData?.volumeUSD ? oneDayData.volumeUSD : 0,
    twoDayData?.volumeUSD ? twoDayData.volumeUSD : 0,
  );
  const [oneDayVolumeUntracked, volumeChangeUntracked, twoDayVolumeUntracked] = get2DayPercentChange(
    data?.untrackedVolumeUSD,
    oneDayData?.untrackedVolumeUSD ? parseFloat(oneDayData?.untrackedVolumeUSD) : 0,
    twoDayData?.untrackedVolumeUSD ? twoDayData?.untrackedVolumeUSD : 0,
  );
  const threeDayVolumeUSD = parseFloat(threeDayData ? data?.volumeUSD - threeDayData?.volumeUSD : data.volumeUSD);

  // set volume properties
  data.oneDayVolumeUSD = parseFloat(oneDayVolumeUSD);
  data.twoDayVolumeUSD = parseFloat(twoDayVolumeUSD);
  data.threeDayVolumeUSD = threeDayVolumeUSD;
  data.volumeChangeUSD = volumeChangeUSD;
  data.oneDayVolumeUntracked = oneDayVolumeUntracked;
  data.twoDayVolumeUntracked = twoDayVolumeUntracked;
  data.volumeChangeUntracked = volumeChangeUntracked;

  // set liquiditry properties
  data.trackedReserveUSD = data.trackedReserveETH * ethPrice;
  data.liquidityChangeUSD = getPercentChange(data.reserveUSD, oneDayData?.reserveUSD);

  // format if pair hasnt existed for a day or a week
  if (!oneDayData && data && data.createdAtBlockNumber > oneDayBlock) {
    data.oneDayVolumeUSD = parseFloat(data.volumeUSD);
  }
  if (!oneDayData && data) {
    data.oneDayVolumeUSD = parseFloat(data.volumeUSD);
  }
  if (!threeDayData && data) {
    data.threeDayVolumeUSD = parseFloat(data.volumeUSD);
  }
  if (data?.token0?.id === "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2") {
    data.token0.name = "Ether (Wrapped)";
    data.token0.symbol = "ETH";
  }
  if (data?.token1?.id === "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2") {
    data.token1.name = "Ether (Wrapped)";
    data.token1.symbol = "ETH";
  }

  const addlMetrics = twoDayData
    ? getMetricsForPositionWindowCustom(
        {
          ...oneDayData,
          token0PriceUSD: ethPrice * oneDayData.token0?.derivedETH,
          token1PriceUSD: ethPrice * oneDayData.token1?.derivedETH,
        },
        {
          ...twoDayData,
          token0PriceUSD: ethPrice * twoDayData?.token0?.derivedETH,
          token1PriceUSD: ethPrice * twoDayData?.token1?.derivedETH,
        },
      )
    : null;

  const addlMetrics2 = threeDayData
    ? getMetricsForPositionWindowCustom(
        {
          ...twoDayData,
          token0PriceUSD: ethPrice * twoDayData?.token0?.derivedETH,
          token1PriceUSD: ethPrice * twoDayData?.token1?.derivedETH,
        },
        {
          ...threeDayData,
          token0PriceUSD: ethPrice * threeDayData?.token0?.derivedETH,
          token1PriceUSD: ethPrice * threeDayData?.token1?.derivedETH,
        },
      )
    : null;
  return { ...data, impLoss24H: addlMetrics?.impLoss || 0, impLoss48H: addlMetrics2?.impLoss || 0 };
}

export async function getUniswapPairData(poolAddress) {
  let newData = await uniswapClient.query({
    query: PAIR_DATA(poolAddress),
    fetchPolicy: "cache-first",
  });
  return newData.data.pairs[0];
}

export async function getBulkPairData(pairList, ethPrice) {
  const [t1, t2, t3] = getTimestampsForChanges();
  let [{ number: b1 }, { number: b2 }, { number: b3 }] = await getBlocksFromTimestamps([t1, t2, t3]);

  try {
    let current = await uniswapClient.query({
      query: PAIRS_BULK,
      variables: {
        allPairs: pairList,
      },
      fetchPolicy: "cache-first",
    });

    let [oneDayResult, twoDayResult, threeDayResult] = await Promise.all(
      [b1, b2, b3].map(async block => {
        let result = uniswapClient.query({
          query: PAIRS_HISTORICAL_BULK(block, pairList),
          fetchPolicy: "no-cache", // there is a bug with the 'cache-first' here where it will fetch the wrong entities after the first fetch (since we are doing a bunch of queries but with diff blocks. This doesn't happen on uniswap-info site since they are using redux to cache the state. therefore, set to no-cache
        });
        return result;
      }),
    );

    console.log(oneDayResult);
    console.log(twoDayResult);

    let oneDayData = oneDayResult?.data?.pairs.reduce((obj, cur, i) => {
      return { ...obj, [cur.id]: cur };
    }, {});

    let twoDayData = twoDayResult?.data?.pairs.reduce((obj, cur, i) => {
      return { ...obj, [cur.id]: cur };
    }, {});

    let threeDayData = threeDayResult?.data?.pairs.reduce((obj, cur, i) => {
      return { ...obj, [cur.id]: cur };
    }, {});

    let pairData = await Promise.all(
      current &&
        current.data.pairs.map(async pair => {
          let data = pair;
          let oneDayHistory = oneDayData?.[pair.id];
          if (!oneDayHistory) {
            let newData = await uniswapClient.query({
              query: PAIR_DATA(pair.id, b1),
              fetchPolicy: "cache-first",
            });
            oneDayHistory = newData.data.pairs[0];
          }
          let twoDayHistory = twoDayData?.[pair.id];
          if (!twoDayHistory) {
            let newData = await uniswapClient.query({
              query: PAIR_DATA(pair.id, b2),
              fetchPolicy: "cache-first",
            });
            twoDayHistory = newData.data.pairs[0];
          }
          let threeDayHistory = threeDayData?.[pair.id];
          if (!threeDayHistory) {
            let newData = await uniswapClient.query({
              query: PAIR_DATA(pair.id, b3),
              fetchPolicy: "cache-first",
            });
            threeDayHistory = newData.data.pairs[0];
          }
          data = parseData(data, oneDayHistory, twoDayHistory, threeDayHistory, ethPrice, b1);
          return data;
        }),
    );
    return pairData;
  } catch (e) {
    console.log(e);
  }
}

/**
 * Gets the current price  of ETH, 24 hour price, and % change between them
 * from Uniswap
 */
export async function fetchUniswapEthPrice() {
  let ethPrice = 0;
  try {
    let result = await uniswapClient.query({
      query: ETH_PRICE(),
      fetchPolicy: "cache-first",
    });
    ethPrice = result?.data?.bundles[0]?.ethPrice;
  } catch (e) {
    console.log(e);
  }

  return [ethPrice];
}

export async function graphPricesUniswap(poolAddress) {
  let graphPrices = [];
  try {
    let result = await uniswapClient.query({
      query: PAIR_GRAPH_PRICES_UNISWAP(poolAddress),
      fetchPolicy: "cache-first",
    });
    const token0Price = result?.data?.pair?.token0Price;
    const token1Price = result?.data?.pair?.token1Price;
    graphPrices = [token0Price, token1Price];
  } catch (e) {
    console.log(e);
  }

  return graphPrices;
}

export async function fetchLpPriceUniswap(poolAddress) {
  let lpPrice = 0;
  try {
    let result = await uniswapClient.query({
      query: UNISWAP_LP_PRICE(poolAddress),
      fetchPolicy: "cache-first",
    });
    if (result.data && result.data.pair.totalSupply > 0) {
      lpPrice = result?.data?.pair?.reserveUSD / result?.data?.pair?.totalSupply;
    }
  } catch (e) {
    console.log(e);
  }

  return [lpPrice];
}

export async function fetchLpUniswapData(poolAddress, timestamp) {
  let data = [];
  try {
    const result = await uniswapClient.query({
      query: UNISWAP_LP_DATA_SINCE_TIMESTAMP(poolAddress, timestamp),
      fetchPolicy: "cache-first",
    });
    if (result.data) {
      data = result?.data?.pairDayDatas;
    }
  } catch (e) {
    console.log(e);
  }
  return data;
}

export const fetchNewestUniswapPoolsData = async () => {
  let data = [];
  try {
    const result = await uniswapClient.query({
      query: UNISWAP_NEWEST_POOLS(),
      fetchPolicy: "cache-first",
    });
    if (result.data) {
      data = result?.data?.pairs;
    }
  } catch (e) {
    console.log(e);
  }
  return data;
};
